//
//  socketClass.swift
//  Hiddy
//
//  Created by APPLE on 28/06/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import Foundation
import SocketIO

var socket = SocketManager(socketURL: URL(string: SOCKET_URL)!, config: [.log(true), .compress])

protocol socketClassDelegate {
    func gotSocketInfo(dict:NSDictionary,type:String)
}

class socketClass {
    static let sharedInstance = socketClass()
     var delegate : socketClassDelegate?
    let localDB = LocalStorage()

    //connect socket
    func connect()  {
        self.disconnect()//disconnect before connect
        socket.defaultSocket.connect()
        socket.defaultSocket.on(clientEvent: .connect) {data, ack in
            // Print("SOCKET CONNECTED SUCCESSFULLY")
            self.connectChat()
            groupSocket.sharedInstance.joinGroups()
            channelSocket.sharedInstance.joinChannels()
            self.addHandler()
            groupSocket.sharedInstance.addGroupHandler()
            StorySocket.sharedInstance.addStoryHandler()
            channelSocket.sharedInstance.addChannelHandler()
            if UserModel.shared.callSocketStatus() == nil{
                callSocket.sharedInstance.CallSocketHandler()
                UserModel.shared.setCallSocket(status: "1")
            }
            
            self.ping()
        }
    }

    //disconnect socet
    func disconnect()  {
        self.offSocketEvents()
        socket.defaultSocket.disconnect()
    }
    
    func destorySocket()  {
        self.disconnect()
        socket.removeSocket(socket.defaultSocket)
    }
    //off all sockets
    func offSocketEvents()  {
        socket.defaultSocket.off("receivechat")
        socket.defaultSocket.off("endchat")
        socket.defaultSocket.off("changeuserimage")
        socket.defaultSocket.off("listentyping")
        socket.defaultSocket.off("viewchat")
        socket.defaultSocket.off("onlinestatus")
        socket.defaultSocket.off("blockstatus")
        socket.defaultSocket.off("groupInvitation")
        socket.defaultSocket.off("msgFromGroup")
        socket.defaultSocket.off("memberExited")
        socket.defaultSocket.off("listenGroupTyping")
        socket.defaultSocket.off("groupDeleted")
        socket.defaultSocket.off("makeprivate")
        socket.defaultSocket.off("msgfromadminchannels")
        socket.defaultSocket.off("Channelcreated")
        socket.defaultSocket.off("receiveChannelInvitation")
        socket.defaultSocket.off("deletechannel")
        
        socket.defaultSocket.off("receivestory")
        socket.defaultSocket.off("storyviewed")
        socket.defaultSocket.off("stroydeleted")
        socket.defaultSocket.off("getbackstatus")
        socket.defaultSocket.off("storyofflinedelete")
//        socket.defaultSocket.off("callCreated")
//        socket.defaultSocket.off("join")
//        socket.defaultSocket.off("joined")
//        socket.defaultSocket.off("bye")
//        socket.defaultSocket.off("created")
//        socket.defaultSocket.off("rtcmessage")
        socket.defaultSocket.off("ping")

    }
    func ping()  {
        socket.defaultSocket.emit("ping","")
    }
    //connect chat
    func connectChat(){
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        requestDict.setValue(NSDate().timeIntervalSince1970 * 1000, forKey: "timestamp")
        socket.defaultSocket.emit("chatbox", requestDict)
    }
    
    //send msg
    func sendMsg(requestDict:NSDictionary) {
         print("SEND MSG \(requestDict)")
        let Dict = requestDict as NSDictionary
        Dict.setValue(UserModel.shared.userName() as String?, forKey: "sender_name")
        socket.defaultSocket.emit("startchat", requestDict)
    }
    //Delete msg
    func deleteMsg(msgDict:NSDictionary, message_id: String, type: String) {
        let requestDict = NSMutableDictionary()
        requestDict.setValue(message_id, forKey: "message_id")
        requestDict.setValue(type, forKey: "delete_status")
        socket.defaultSocket.emit("deletechat", requestDict)
    }

    //block contact
    func blockContact(contact_id:String,type:String){
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(contact_id, forKey: "receiver_id")
        requestDict.setValue(type, forKey: "type")
        socket.defaultSocket.emit("block", requestDict)
        if type == "block" {
            localDB.updateBlockedStatus(contact_id: contact_id, type: "blockedByMe",value:"1")
        }else if type == "unblock"{
            localDB.updateBlockedStatus(contact_id: contact_id, type: "blockedByMe",value:"0")
        }
    }
    //send typing status
    func sendTypingStatus(requestDict:NSDictionary) {
        socket.defaultSocket.emit("typing", requestDict)
    }
    //send online status
    func sendOnlineStatus(contact_id:String){
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        requestDict.setValue(contact_id, forKey: "contact_id")
        socket.defaultSocket.emit("online", requestDict)
    }
    //send request to chat received
    func chatReceived(msgDict:NSDictionary) {
        let requestDict = NSMutableDictionary()
        requestDict.setValue(msgDict.value(forKey: "sender_id"), forKey: "sender_id")
        requestDict.setValue(msgDict.value(forKey: "receiver_id"), forKey: "receiver_id")
        requestDict.setValue(msgDict.value(forKeyPath: "message_data.message_id"), forKey: "message_id")
        socket.defaultSocket.emit("chatreceived", requestDict)
    }
    //send request to chat read
    func chatRead(sender_id:String,receiver_id:String) {
        let requestDict = NSMutableDictionary()
        requestDict.setValue(sender_id, forKey: "sender_id")
        requestDict.setValue(receiver_id, forKey: "receiver_id")
        requestDict.setValue("\(sender_id)\(receiver_id)", forKey: "chat_id")
        socket.defaultSocket.emit("chatviewed", requestDict)
    }
    //get rencent single message
    //its called when come from offline mode
    func getRecentMsg()  {
        let userObj = UserWebService()
        
        userObj.recentChat(onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                let msgArray:NSArray = response.value(forKey: "result") as! NSArray
                var msgTempDict = NSDictionary()

                for msg in msgArray {
                    let msgTempArray = NSMutableArray.init(array: [msg])
                    msgTempDict = msgTempArray.object(at: 0) as! NSDictionary
                    if msgTempDict.value(forKey: "sender_id") != nil{
                    let contact_id:String = msgTempDict.value(forKey: "sender_id") as! String
                    if (UserModel.shared.contactIDs()?.contains(contact_id))!{
                        self.addMsg(contact_id: contact_id,msgDict:msgTempDict)
                    }else{ // if user not in contact
                        let userObj = UserWebService()
                        userObj.otherUserDetail(contact_id: contact_id, onSuccess: {response in
                            let status:String = response.value(forKey: "status") as! String
                            if status == STATUS_TRUE{
                                let phone_no :NSNumber = response.value(forKey: "phone_no") as! NSNumber
                                let cc = response.value(forKey: "country_code") as! Int

                                self.localDB.addContact(userid: contact_id,
                                                        contactName: ("+\(cc) " + "\(phone_no)"),
                                    userName: response.value(forKey: "user_name") as! String,
                                    phone: "\(phone_no)",
                                    img: response.value(forKey: "user_image") as! String,
                                    about: response.value(forKey: "about") as! String,
                                    type: EMPTY_STRING,
                                    mutual:response.value(forKey: "contactstatus") as! String,
                                    privacy_lastseen: response.value(forKey: "privacy_last_seen") as! String,
                                    privacy_about: response.value(forKey: "privacy_about") as! String,
                                    privacy_picture: response.value(forKey: "privacy_profile_image") as! String, countryCode: String(cc))
                                
                                self.addMsg(contact_id: contact_id,msgDict:msgTempDict )
                            }
                        })
                    }
                    }
                }
                self.delegate?.gotSocketInfo(dict: response,type: "recentMsg")
            }
        })
        
//        if !Utility.shared.isConnectedToNetwork() {
//            let dict = NSDictionary()
//            self.delegate?.gotSocketInfo(dict:dict, type: "offlineRefresh")
//        }
    }
  // add single message to local storage
    func addMsg(contact_id:String,msgDict: NSDictionary)  {
        
        let unreadcount = localDB.getUnreadCount(contact_id: msgDict.value(forKey: "sender_id")as! String)
            localDB.addRecent(contact_id:contact_id , msg_id: msgDict.value(forKey: "message_id") as! String, unread_count: "\(unreadcount)",time: msgDict.value(forKey: "chat_time") as! String)
            let reqDict = NSMutableDictionary()
            reqDict.setValue(msgDict, forKey: "message_data")
            reqDict.setValue(msgDict.value(forKey: "sender_id"), forKey: "sender_id")
            reqDict.setValue(msgDict.value(forKey: "receiver_id"), forKey: "receiver_id")
        let chat_id:String = "\(UserModel.shared.userID()!)\(contact_id)"

            Utility.shared.addToLocal(requestDict: reqDict, chat_id: chat_id, contact_id: contact_id)
            socketClass.sharedInstance.chatReceived(msgDict: reqDict)
        
        self.delegate?.gotSocketInfo(dict: msgDict,type: "checkCurrentChat")
    }
    
    func refreshView()  {
        let dict = NSMutableDictionary()
        self.delegate?.gotSocketInfo(dict: dict,type: "offlineRefresh")

    }
    //video upload
    func uploadChatVideo(fileData:Data,type:String,msg_id:String,requestDict:NSDictionary)  {
        let uploadObj = UploadServices()
        uploadObj.uploadFiles(fileData: fileData, type: type, user_id: UserModel.shared.userID()! as String, docuName: "Video",msg_id: msg_id,api_type:"private", onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                self.localDB.updateDownload(msg_id: msg_id,status:"1")
                self.localDB.updateVideoURL(msg_id: msg_id, attachment: response.value(forKey: "user_image") as! String)
                var msgdict = NSMutableDictionary()
                let time = NSDate().timeIntervalSince1970
                msgdict = requestDict.value(forKey: "message_data") as! NSMutableDictionary
                msgdict.removeObject(forKey: "attachment")
                msgdict.removeObject(forKey: "chat_time")
                msgdict.setValue(response.value(forKey: "user_image"), forKey: "attachment")
                msgdict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                msgdict.setValue(requestDict.value(forKey: "sender_id"), forKey: "sender_id")
                msgdict.setValue(requestDict.value(forKey: "receiver_id"), forKey: "receiver_id")
                let newDict = NSMutableDictionary.init(dictionary: requestDict)
                newDict.removeObject(forKey: "message_data")
                newDict.setValue(msgdict, forKey: "message_data")
                socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                self.delegate?.gotSocketInfo(dict: newDict,type: "videoUploadStatus")
            }
        })
    }
    
    // Response for receive, read message
    func addHandler()  {
        socket.defaultSocket.on("pong") { ( data, ack) -> Void in
            self.ping()
        }
        socket.defaultSocket.on("receivechat") { ( data, ack) -> Void in
             print("SOCKET NEW MESSAGE \(data)")
        
            let msgList:NSArray = data as NSArray
            let msgDict:NSDictionary = msgList.object(at: 0) as! NSDictionary
            let contact_id : String = msgDict.value(forKey: "sender_id") as! String
            let chat_id:String = "\(UserModel.shared.userID()!)\(contact_id)"
//            let msg_type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            if (UserModel.shared.contactIDs()?.contains(contact_id))!{
                socketClass.sharedInstance.chatReceived(msgDict: msgDict)
                Utility.shared.addToLocal(requestDict: msgDict, chat_id: chat_id, contact_id: contact_id)
                self.delegate?.gotSocketInfo(dict: msgDict,type: "receivechat")
            }else{
                let userObj = UserWebService()
                userObj.otherUserDetail(contact_id: contact_id, onSuccess: {response in
                    let status:String = response.value(forKey: "status") as! String
                    if status == STATUS_TRUE{
                        let phone_no :NSNumber = response.value(forKey: "phone_no") as! NSNumber
                        let cc = response.value(forKey: "country_code") as! Int
                        self.localDB.addContact(userid: contact_id,
                                                contactName: ("+\(cc) " + "\(phone_no)"),
                                        userName: response.value(forKey: "user_name") as! String,
                                                phone: "\(phone_no)",
                                                img: response.value(forKey: "user_image") as! String,
                                                about: response.value(forKey: "about") as! String,
                                                type: EMPTY_STRING,
                            mutual:response.value(forKey: "contactstatus") as! String,
                            privacy_lastseen: response.value(forKey: "privacy_last_seen") as! String,
                            privacy_about: response.value(forKey: "privacy_about") as! String,
                            privacy_picture: response.value(forKey: "privacy_profile_image") as! String, countryCode: String(cc))
                        
                        socketClass.sharedInstance.chatReceived(msgDict: msgDict)
                        Utility.shared.addToLocal(requestDict: msgDict, chat_id: chat_id, contact_id: contact_id)
                        self.delegate?.gotSocketInfo(dict: msgDict,type: "receivechat")
                    }
                })
            }
        }
        //change profile pic
        socket.defaultSocket.on("changeuserimage") { ( data, ack) -> Void in
            // Print("SOCKET CHANGE PIC \(data)")
            let msgList:NSArray = data as NSArray
            let msgDict:NSDictionary = msgList.object(at: 0) as! NSDictionary
            self.localDB.replacePic(contact_id: msgDict.value(forKey: "user_id") as! String, img: msgDict.value(forKey: "user_image") as! String)
            self.delegate?.gotSocketInfo(dict: msgDict,type: "changeuserimage")
        }
        //typing listener
        socket.defaultSocket.on("listentyping") { ( data, ack) -> Void in
            let listArray:NSArray = data as NSArray
            let detailDict:NSDictionary = listArray.object(at: 0) as! NSDictionary
            self.delegate?.gotSocketInfo(dict: detailDict,type: "listentyping")
        }
        
        //chat received response
        socket.defaultSocket.on("endchat") { ( data, ack) -> Void in
            // update particular chat by message id
            // Print("SOCKET DELIVERED RESPONSE \(data)")
            let listArray:NSArray = data as NSArray
            let detailDict:NSDictionary = listArray.object(at: 0) as! NSDictionary
            let localDB = LocalStorage()
            if detailDict.value(forKey: "message_id") != nil{
            localDB.readStatus(id: detailDict.value(forKey: "message_id") as! String, status: "2", type: "message")
            self.delegate?.gotSocketInfo(dict: detailDict,type: "endchat")
            }
        }
        
        //chat read response
        socket.defaultSocket.on("viewchat") { ( data, ack) -> Void in
            // Print("SOCKET READ RESPONSE \(data)")
            let listArray:NSArray = data as NSArray
            let detailDict:NSDictionary = listArray.object(at: 0) as! NSDictionary
//            sleep(1)
            if detailDict.value(forKey: "chat_id") != nil{
            self.localDB.readStatus(id: detailDict.value(forKey: "chat_id") as! String, status: "3", type: "chat")
            self.delegate?.gotSocketInfo(dict: detailDict,type: "viewchat")
            }
        }
        

        
        //check contact online status
        socket.defaultSocket.on("onlinestatus") { ( data, ack) -> Void in
            let list:NSArray = data as NSArray
            let resultDict:NSDictionary = list.object(at: 0) as! NSDictionary
            self.delegate?.gotSocketInfo(dict: resultDict,type: "onlinestatus")
        }
        //check contact online status
        socket.defaultSocket.on("blockstatus") { ( data, ack) -> Void in
            let list:NSArray = data as NSArray
            let resultDict:NSDictionary = list.object(at: 0) as! NSDictionary
            let blockType = resultDict.value(forKey: "type") as! String
            if blockType == "block"{
                self.localDB.updateBlockedStatus(contact_id: resultDict.value(forKey: "sender_id") as! String, type: "blockedMe",value:"1" )
            }else if blockType == "unblock"{
                self.localDB.updateBlockedStatus(contact_id: resultDict.value(forKey: "sender_id") as! String, type: "blockedMe",value:"0" )
            }
            self.delegate?.gotSocketInfo(dict: resultDict,type: "blockstatus")
        }
        
        //check contact offline delivered status
        socket.defaultSocket.on("offlinedeliverystatus") { ( data, ack) -> Void in
            let list:NSArray = data as NSArray
            let resultDict:NSDictionary = list.object(at: 0) as! NSDictionary
            // Print("delivery stt \(resultDict)")
            let msgArray :NSArray = resultDict.value(forKey: "messages") as! NSArray
            for msg in msgArray{
                let dict : NSDictionary = msg as! NSDictionary
                socketClass.sharedInstance.chatReceived(msgDict: dict)
                self.localDB.readStatus(id: dict.value(forKey: "message_id") as! String, status: "2", type: "message")
            }
        }
        
        //check contact offline read status
        socket.defaultSocket.on("offlinereadstatus") { ( data, ack) -> Void in
            let list:NSArray = data as NSArray
            let resultDict:NSDictionary = list.object(at: 0) as! NSDictionary
            // Print("read stt \(resultDict)")
            let chatArray :NSArray = resultDict.value(forKey: "chats") as! NSArray
            for chat in chatArray{
                let dict : NSDictionary = chat as! NSDictionary
                socketClass.sharedInstance.chatRead(sender_id: dict.value(forKey: "sender_id")! as! String, receiver_id: dict.value(forKey: "receiver_id")! as! String)
                self.localDB.readStatus(id: dict.value(forKey: "chat_id") as! String, status: "3", type: "chat")
            }
       }
        
        //check privacy details
        socket.defaultSocket.on("makeprivate") { ( data, ack) -> Void in
            let list:NSArray = data as NSArray
            let dict:NSDictionary = list.object(at: 0) as! NSDictionary
            let user_id:String = dict.value(forKey: "user_id") as! String
            if user_id != UserModel.shared.userID()! as String {
            // Print("PRIVACY DICT \(dict)")
            self.localDB.updatePrivacy(user_id: user_id,
                                       lastseen: dict.value(forKey: "privacy_last_seen") as! String,
                                       about: dict.value(forKey: "privacy_about") as! String,
                                       profile_pic: dict.value(forKey: "privacy_profile_image") as! String)
                self.delegate?.gotSocketInfo(dict: dict,type: "makeprivate")
            }
            
        }
}
}

//
//  Config.swift
//  HSTaxiUserApp
//
//  Created by APPLE on 09/03/18.
//  Copyright © 2018 APPLE. All rights reserved.

import Foundation
import UIKit

//MARK: Default
let DEFAULT_LANGUAGE = "English"

//MARK: message encryption key.
let ENCRYPT_KEY = "Hiddy123!@#"

// MARK: Document path Name
let DOCUMENT_PATH = "Status"

//MARK: Setup Socket
let SOCKET_URL = "YOUR_SOCKET_URL" // ex: "http://example.com:8085"

//MARK: CALL servers
let APP_RTC_URL = "http://turn.hitasoft.in:8080"

//MARK: Google API key
let GOOGLE_API_KEY = "AIzaSyB-lbgPtQ93DD1AHJiKulXp04pxY7QwC3U"

//MARK: API Details
//Base URL
let BASE_URL = "YOUR_API_BASE_URL"// Demo "http://example.com:3002/service/"
let IMAGE_BASE_URL = "YOUR_IMAGE_BASE_URL" // Demo "http://example.com:3005/media/"

let IMAGE_SUB_URL = "chats/"
let USERS_SUB_URL = "users/"

let ITUNES_URL = "https://itunes.apple.com/us/app/hiddy/id1421087138?ls=1&mt=8"

//sub URL
let SIGN_IN_API = "signin"
let UPDATE_CONTACTS_API = "updatemycontacts"
let PROFILE_PIC_API = "upmyprofile"
let UPLOAD_FILES_API = "upmychat"
let UPDATE_PROFILE_API = "updatemyprofile"
let BLOCKED_USERLIST_API = "getblockstatus"
let RECENT_CHATS_API = "recentchats"
let PUSH_SIGNIN_API = "pushsignin"
let PUSH_SIGNOUT_API = "pushsignout"
let OTHER_USER_DETAIL_API = "getuserprofile"
let HELP_API = "helps"
let CHECK_DEVICE_API =   "deviceinfo"
let CHAT_RECEIVED_API = "chatreceived"
let UPDATE_PRIVACY_API = "updatemyprivacy"
let SAVE_MY_CONTACT = "savemycontacts"

//GROUP
let CHANGE_GROUP_ICON_API = "modifyGroupimage"
let GROUP_INVITES = "groupinvites"
let MODIFY_GROUP_INFO = "modifyGroupinfo"
let ADD_NEW_MEMBERS = "modifyGroupmembers"
let GROUP_RECENT_CHATS = "recentgroupchats"
let GROUP_CHAT_RECEIVED_API = "groupchatreceived"
let GROUP_INFO_API = "groupinfo"
let UPLOAD_GROUP_FILES_API = "upmygroupchat"
let MY_GROUPS = "MyGroups"
let DELETE_ACCOUNT = "deleteMyAccount"
let CHANGE_NO_API = "changeMyNumber"
let VERIFY_NO_API = "verifyMyNumber"

//CHANNEL
let ADMIN_CHANNELS = "adminchannels"
let GET_ADMIN_CHANNEL_MSG = "msgfromadminchannels"
let CHANGE_CHANNEL_ICON_API = "modifyChannelImage"
let CHANNEL_INFO = "channelinfo"
let UPDATE_CHANNEL_API = "updatemychannel"
let MY_CHANNEL_API = "MyChannels"
let MY_SUBSCRIBED_CHANNEL = "MySubscribedChannels"
let UPLOAD_CHANNEL_FILES_API = "upmychannelchat"
let RECENT_CHANNEL_INVITES = "recentChannelInvites"
let RECENT_CHANNEL_CHATS = "recentChannelChats"
let ALL_CHANNELS = "AllPublicChannels"
let ALL_SUBSCRIBER = "channelSubscribers"
let REPORT_CHANNEL = "reportchannel"

//update
let VERSION_API = "checkforupdates"
//Response status
let STATUS_TRUE = "true"
let STATUS_FALSE = "false"

//Push notification device mode
let DEVICE_MODE = "1" // 0 - DEVELOPMENT, 1 - PRODUCTION
let RESET_BADGE = "resetunread"


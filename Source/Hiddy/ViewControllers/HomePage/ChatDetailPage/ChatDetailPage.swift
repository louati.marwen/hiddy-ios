
//
//  ChatDetailPage.swift
//  Hiddy
//
//  Created by APPLE on 01/06/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit
import MapKit
import MobileCoreServices
import Contacts
import ContactsUI
import AVKit
import GSImageViewerController
import SwiftWebVC
import Photos
import Lottie
import AssetsLibrary
import GrowingTextView
import AVFoundation
import Shimmer
import iRecordView
import IQKeyboardManagerSwift
import PhoneNumberKit
class ChatDetailPage: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,fetchLocationDelegate,UIDocumentPickerDelegate,CNContactPickerDelegate,CNContactViewControllerDelegate,UIDocumentInteractionControllerDelegate,socketClassDelegate,alertDelegate,UIGestureRecognizerDelegate,forwardDelegate,GrowingTextViewDelegate,UITextViewDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate, deleteAlertDelegate {
    
    @IBOutlet weak var attachmentView: UIView!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    @IBOutlet weak var recorderView: UIView!
    @IBOutlet weak var bottomStackView: UIStackView!
    var localCallDB = CallStorage()
    var docController:UIDocumentInteractionController!
    var numberOfPhotos: NSNumber?
    var msgArray = NSMutableArray()
    var tempMsgs:AnyObject?
    var finalArray = NSMutableArray()
    var chatDetailDict = NSDictionary()
    let localDB = LocalStorage()
    var onlineTimer = Timer()
    var chat_id = String()
    var galleryType = String()
    var attachmentShow = Bool()
    var isFetch = Bool()
    var timeStamp = Date()
    var contactStore = CNContactStore()
    let contactPicker = CNContactPickerViewController()
    var contact_id = String()
    var menuArray = NSMutableArray()
    var blockByMe = String()
    var blockedMe = String()
    var viewType = String()
    var startTyping = Bool()
    //    var selectedIndexPath = IndexPath()
    var selectedIndexArr = [IndexPath]()
    var selectedIdArr = [String]()
    
    var longPressGesture = UILongPressGestureRecognizer()
    var receiverId : String!
    var heightAtIndexPath = NSMutableDictionary()
    var isScrollBottom = Bool()
    var statusSizeArr = [CGFloat]()
    var textSizeArr = [CGFloat]()
    let del = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var forwardIconView: UIView!
    @IBOutlet weak var attachmentIcon: UIImageView!
    @IBOutlet weak var backArrowIcon: UIImageView!
    @IBOutlet weak var deleteIcon: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet var copyIcon: UIImageView!
    @IBOutlet var copyBtn: UIButton!
    @IBOutlet var menuBtn: UIButton!
    @IBOutlet var bootomInputView: UIView!
    @IBOutlet var contactNameLbl: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var lastSeenLbl: UILabel!
    @IBOutlet var toolContainerView: UIView!
    @IBOutlet var messageTextView: GrowingTextView!
    @IBOutlet var msgTableView: UITableView!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var sendImgView: UIImageView!
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var attachmentMenuView: UIView!
    @IBOutlet var forwardView: UIView!
    
    //New Customize
    
    @IBOutlet weak var record_btn_ref: RecordButton!
    
    var audioRecorder: AVAudioRecorder!
    //var audioPlayer = AVAudioPlayer()
    var meterTimer:Timer!
    var isAudioRecordingGranted: Bool!
    var isRecording = false
    var ishold = false
    var isPlaying = false
    var updater : CADisplayLink! = nil
    var senderaudioCell = SenderAudioCell()
    var audioPlayer:AVAudioPlayer!
    var receiveraudioCell = ReceiverVoiceCell()
    
    var counter = 0
    var timer = Timer()
    var tag_value = Int()
    var str_value_tofind_which_voiceCell = String()
    var isSwipeCalled = Bool()
    var currentAudioMsgID = String()
    var currentAudioStatus = String()
    var VoiceRecordingSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "Voice_record", ofType: ".wav")!)
    var audioPlayer_VoiceRecord:AVAudioPlayer!
    
    var chatCount = 0
    var selectedDict = [messageModel.message]()
    var keyboardTypeUpdate = 0
    let recordView = RecordView()
    var scrollTag = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.forwardView.isHidden = true

        self.msgTableView.rowHeight = UITableView.automaticDimension
        self.msgTableView.estimatedRowHeight = 150
        self.msgTableView.sectionHeaderHeight = UITableView.automaticDimension
        self.msgTableView.estimatedSectionHeaderHeight = 40
        self.configMsgField()
        self.customAudioRecordView()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        }
        catch {
            
        }
        self.recordAudioView()
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.didEnterBackgroundNotification, object: nil)

//        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:))))
    }

    @objc func willResignActive() {
//        self.onCancel()
//        self.recorderView.isHidden = true
//        self.recordView.isHidden = true
//        if(isRecording){
//            if(audioRecorder.isRecording){
//                audioRecorder.stop()
//                audioRecorder.deleteRecording()
//            }
//        }
//        isRecording = false
//        let requestDict = NSMutableDictionary()
//        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
//        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
//        let sender_id:String = UserModel.shared.userID()! as String
//        let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String
//
//        requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
//        requestDict.setValue("untyping", forKey: "type")
//        if self.blockedMe != "1" && blockByMe == "0"{
//            socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
//        }
//        self.attachmentView.isHidden = false
//        messageTextView.isHidden = false

    }
    func recordAudioView() {
        
        recordView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(recordView)
        recordView.trailingAnchor.constraint(equalTo: recorderView.trailingAnchor, constant: -15).isActive = true
        recordView.leadingAnchor.constraint(equalTo: recorderView.leadingAnchor, constant: 15).isActive = true
        recordView.topAnchor.constraint(equalTo: recorderView.topAnchor, constant: 0).isActive = true
        recordView.bottomAnchor.constraint(equalTo: recorderView.bottomAnchor, constant: 0).isActive = true
        record_btn_ref.recordView = recordView
        recordView.delegate = self
        //enable/disable Record Sounds
        recordView.isSoundEnabled = true

        recordView.durationTimerColor = TEXT_TERTIARY_COLOR
        recordView.slideToCancelArrowImage = nil
        recordView.slideToCancelText = Utility.shared.getLanguage()?.value(forKey: "slide_cancel") as? String ?? ""
        recordView.slideToCancelTextColor = TEXT_TERTIARY_COLOR
//
    }
    func customAudioRecordView() {
        self.check_record_permission()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.recorderView.isHidden = true
        self.recordView.isHidden = true
        IQKeyboardManager.shared.enable = false
        setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = true
        self.initialSetup()
        self.changeRTLView()
    }
    func changeRTLView() {
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.msgTableView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.backArrowIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.messageTextView.textAlignment = .right
        }
        else {
            self.msgTableView.transform = .identity
            self.backArrowIcon.transform = .identity
            self.messageTextView.textAlignment = .left
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        //        self.viewDidLayoutSubviews()
        //        self.initialSetup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.localDB.readStatus(id: self.contact_id, status: "3", type: "sender")
        self.localDB.updateRecent(chat_id: self.chat_id)
        self.onlineTimer.invalidate()
        Utility.shared.setBadge(vc: self)
        IQKeyboardManager.shared.enable = true
    }
    //scroll to bottom
    func scrollToBottom(){
        if self.msgArray.count != 0 {
            DispatchQueue.main.async {
                if self.msgArray.count != 0 {
                    let indexPath = IndexPath(row: self.msgArray.count - 1, section: 0)
                    self.msgTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }else {
                }
            }
        }
        else {
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50.0 //Maximum possible cell height
    }
    override func viewDidLayoutSubviews() {
    }
    
    //set up initial details
    func initialSetup()  {
        self.chatDetailDict = localDB.getContact(contact_id: self.contact_id)
        //connect socket
        socketClass.sharedInstance.delegate = self
        isScrollBottom = false
        //check online
        self.updateOnlineStatus()
        onlineTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.updateOnlineStatus), userInfo: nil, repeats: true)
        contactPicker.delegate = self
        //self.attachmentShow = false
        self.startTyping = false
        self.isFetch =  false
        self.toolContainerView.elevationEffectOnBottom()
        self.contactNameLbl.config(color: TEXT_PRIMARY_COLOR, size: 20, align: .left, text: EMPTY_STRING)
        if Utility.shared.isConnectedToNetwork() {
            if IS_IPHONE_5{
                self.lastSeenLbl.config(color: TEXT_SECONDARY_COLOR, size: 10, align: .left, text: EMPTY_STRING)
            }else{
                self.lastSeenLbl.config(color: TEXT_SECONDARY_COLOR, size: 13, align: .left, text: "tap_here_info")
            }
        }else{
            self.lastSeenLbl.config(color: TEXT_SECONDARY_COLOR, size: 15, align: .left, text: EMPTY_STRING)
        }
        self.profilePic.rounded()
        //send btn
        self.sendBtn.cornerRoundRadius()
        if Utility.shared.checkEmptyWithString(value: messageTextView.text) {
            self.configSendBtn(enable: false)
            self.ConfigVoiceBtn(enable: true)
            
        }else{
            self.configSendBtn(enable: true)
            self.ConfigVoiceBtn(enable:false)
            
        }
        //tap to dismiss keyboard
        self.contactNameLbl.text = self.chatDetailDict.value(forKey: "contact_name") as? String
        //register cell nibs
        self.registerCells()
        //keyboard manager
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //get chat msgs from local db
        chat_id = "\(UserModel.shared.userID()!)\(self.chatDetailDict.value(forKey: "user_id")!)"
        self.refresh(type:"scroll")
        
        //sent viewed status to sender
        socketClass.sharedInstance.chatRead(sender_id:self.contact_id, receiver_id:UserModel.shared.userID()! as String)
        self.localDB.readStatus(id: self.contact_id, status: "3", type: "sender")
        self.localDB.updateRecent(chat_id: self.chat_id)
        Utility.shared.setBadge(vc: self)
        
        blockByMe = self.chatDetailDict.value(forKey: "blockedByMe") as! String
        blockedMe = self.chatDetailDict.value(forKey: "blockedMe") as! String
        DispatchQueue.main.async {
            self.configBlockedStatus()
            self.configMuteStatus()
            self.configFavStatus()
        }
        
        setupLongPressGesture()
    }
    
    
    
    //config mute status
    func configMuteStatus()  {
        self.chatDetailDict =  self.localDB.getContact(contact_id: self.contact_id)
        let mute:String = self.chatDetailDict.value(forKey: "mute") as! String
        self.menuArray.removeObject(at: 0)
        if mute == "0"{
            self.menuArray.insert(Utility.shared.getLanguage()?.value(forKey: "mute_notify") as! String, at: 0)
        }else if mute == "1"{
            self.menuArray.insert(Utility.shared.getLanguage()?.value(forKey: "unmute_notify") as! String, at: 0)
        }
    }
    //config fav status
    func configFavStatus()  {
        self.chatDetailDict =  self.localDB.getContact(contact_id: self.contact_id)
        let fav:String = self.chatDetailDict.value(forKey: "favourite") as! String
        if fav == "0"{
            if self.menuArray.contains(Utility.shared.getLanguage()?.value(forKey: "remove_favourite")as! String){
                self.menuArray.remove(Utility.shared.getLanguage()?.value(forKey: "remove_favourite") as! String)
            }
            self.menuArray.add(Utility.shared.getLanguage()?.value(forKey: "add_favourite")as! String)
        }else if fav == "1"{
            if self.menuArray.contains(Utility.shared.getLanguage()?.value(forKey: "add_favourite") as! String){
                self.menuArray.remove(Utility.shared.getLanguage()?.value(forKey: "add_favourite") as! String)
            }
            self.menuArray.add(Utility.shared.getLanguage()?.value(forKey: "remove_favourite") as! String)
        }
    }
    
    //set up message text view
    func configMsgField()  {
        messageTextView.layer.borderWidth  = 1.0
        messageTextView.layer.borderColor = TEXT_TERTIARY_COLOR.cgColor
        messageTextView.font = UIFont.systemFont(ofSize: 18)
        messageTextView.textContainer.lineFragmentPadding = 20
        messageTextView.delegate = self
        
        messageTextView.trimWhiteSpaceWhenEndEditing = true
        messageTextView.placeholder = Utility.shared.getLanguage()?.value(forKey: "say_something") as? String
        messageTextView.placeholderColor = TEXT_TERTIARY_COLOR
        messageTextView.minHeight = 30.0
        messageTextView.maxHeight = 150.0
        messageTextView.layer.cornerRadius = 20.0
        messageTextView.textAlignment = .left
        messageTextView.isUserInteractionEnabled = true
        
        recorderView.layer.borderWidth  = 1.0
        recorderView.layer.borderColor = TEXT_TERTIARY_COLOR.cgColor
        recorderView.layer.cornerRadius = 20.0
    }
    //config blocked Status
    func configBlockedStatus(){
        self.chatDetailDict = self.localDB.getContact(contact_id: contact_id)
        let contact_name:String = chatDetailDict.value(forKey: "contact_name") as? String ?? ""
        let phone_no:String = chatDetailDict.value(forKey: "user_phoneno") as? String ?? ""
        let cc: String = chatDetailDict.value(forKey: "countrycode") as? String ?? ""
        if blockByMe == "1"{
            self.lastSeenLbl.isHidden = false
            self.menuArray = [Utility.shared.getLanguage()?.value(forKey: "mute_notify") as! String,Utility.shared.getLanguage()?.value(forKey: "unblock") as! String,Utility.shared.getLanguage()?.value(forKey: "clear_all")as! String]
        }else if blockByMe == "0"{
            self.lastSeenLbl.isHidden = false
            self.menuArray = [Utility.shared.getLanguage()?.value(forKey: "mute_notify") as! String,Utility.shared.getLanguage()?.value(forKey: "block")as! String,Utility.shared.getLanguage()?.value(forKey: "clear_all")as! String]
        }
        
        if contact_name == "+\(cc) \(phone_no)" {
            self.menuArray.add(Utility.shared.getLanguage()?.value(forKey: "add_to_contact")as! String)
        }
        
        if blockedMe == "1" {
            self.profilePic.image = #imageLiteral(resourceName: "profile_placeholder")
            self.lastSeenLbl.isHidden = true
        }else if blockedMe == "0" && blockByMe == "1"{
            self.lastSeenLbl.isHidden = false
            self.configPrivacySettings()
        }else if blockedMe == "0"{
            self.profilePic.image = #imageLiteral(resourceName: "profile_placeholder")
            self.lastSeenLbl.isHidden = false
        }
        if blockedMe == "0" && blockByMe == "0"{
            self.lastSeenLbl.isHidden = false
            self.configPrivacySettings()
        }
        
        //        self.menuArray.add("Favourite")
    }
    //account setting validation
    func configPrivacySettings() {
        self.chatDetailDict = self.localDB.getContact(contact_id: contact_id)
        let privacy_image:String = chatDetailDict.value(forKey: "privacy_image") as? String ?? ""
        let mutual:String = chatDetailDict.value(forKey: "mutual_status") as? String ?? ""
        let privacy_lastseen:String = chatDetailDict.value(forKey: "privacy_lastseen") as? String ?? ""
        let imageName:String = chatDetailDict.value(forKey: "user_image") as? String ?? ""
        // profile pic
        DispatchQueue.main.async {
            if privacy_image == "nobody"{
                self.profilePic.image =  #imageLiteral(resourceName: "profile_placeholder")
            }else if privacy_image == "everyone"{
                self.profilePic.sd_setImage(with: URL(string:"\(IMAGE_BASE_URL)\(USERS_SUB_URL)\(imageName)"), placeholderImage:  #imageLiteral(resourceName: "profile_placeholder"))
            }else if privacy_image == "mycontacts"{
                if mutual == "true"{
                    self.profilePic.sd_setImage(with: URL(string:"\(IMAGE_BASE_URL)\(USERS_SUB_URL)\(imageName)"), placeholderImage:  #imageLiteral(resourceName: "profile_placeholder"))
                }else{
                    self.profilePic.image =  #imageLiteral(resourceName: "profile_placeholder")
                }
            }
            
        }
        //last seen
        if privacy_lastseen == "nobody"{
            self.lastSeenLbl.isHidden = true
            //            self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
        }else if privacy_lastseen == "everyone"{
            self.lastSeenLbl.isHidden = false
            
        }else if privacy_lastseen == "mycontacts"{
            if mutual == "true"{
                self.lastSeenLbl.isHidden = false
            }else{
                self.lastSeenLbl.isHidden = true
                //                self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
            }
        }
    }
    
    
    //register table view cells
    func registerCells()  {
        msgTableView.register(UINib(nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
        msgTableView.register(UINib(nibName: "SenderImageCell", bundle: nil), forCellReuseIdentifier: "SenderImageCell")
        msgTableView.register(UINib(nibName: "ReceiverImageCell", bundle: nil), forCellReuseIdentifier: "ReceiverImageCell")
        
        msgTableView.register(UINib(nibName: "SenderAudioCell", bundle: nil), forCellReuseIdentifier: "SenderAudioCell")
        msgTableView.register(UINib(nibName: "ReceiverVoiceCell", bundle: nil), forCellReuseIdentifier: "ReceiverVoiceCell")
        
        msgTableView.register(UINib(nibName: "SenderVideoCell", bundle: nil), forCellReuseIdentifier: "SenderVideoCell")
        msgTableView.register(UINib(nibName: "ReceiverVideoCell", bundle: nil), forCellReuseIdentifier: "ReceiverVideoCell")
        
        msgTableView.register(UINib(nibName: "SenderLocCell", bundle: nil), forCellReuseIdentifier: "SenderLocCell")
        msgTableView.register(UINib(nibName: "ReceiverLocCell", bundle: nil), forCellReuseIdentifier: "ReceiverLocCell")
        
        msgTableView.register(UINib(nibName: "SenderContact", bundle: nil), forCellReuseIdentifier: "SenderContact")
        msgTableView.register(UINib(nibName: "ReceiverContact", bundle: nil), forCellReuseIdentifier: "ReceiverContact")
        
        msgTableView.register(UINib(nibName: "SenderDocuCell", bundle: nil), forCellReuseIdentifier: "SenderDocuCell")
        msgTableView.register(UINib(nibName: "ReceiverDocuCell", bundle: nil), forCellReuseIdentifier: "ReceiverDocuCell")
        
        msgTableView.register(UINib(nibName: "dateStickyCell", bundle: nil), forCellReuseIdentifier: "dateStickyCell")
        
        msgTableView.register(UINib(nibName: "ReceiverAudioCell", bundle: nil), forCellReuseIdentifier: "ReceiverAudioCell")
        msgTableView.register(UINib(nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
        msgTableView.register(UINib(nibName: "StatusReplyTableViewCell", bundle: nil), forCellReuseIdentifier: "StatusReplyTableViewCell")
        msgTableView.register(UINib(nibName: "ChatDetailsSectionTableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ChatDetailsSectionTableViewCell")
        
    }
    func setupLongPressGesture() {
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1 // 1 second press
        longPressGesture.delaysTouchesBegan = true
        self.msgTableView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        //get touch point
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        let touchPoint = gestureRecognizer.location(in: self.msgTableView)
        let selectedIndexPath = self.msgTableView.indexPathForRow(at: touchPoint)
        //check if selected is empty or not
        if selectedIndexPath?.count != 0 && self.selectedIdArr.count == 0 && selectedIndexPath != nil{
            //get msg data based on the selection
            var dict:messageModel.message = self.msgArray.object(at: selectedIndexPath?.row ?? 0) as! messageModel.message
            //arrange msg values
            let msg_type = dict.message_data.value(forKey: "message_type") as! String
            let id = dict.message_data.value(forKey: "message_id") as! String
            if msg_type != "date_sticky"{
                if self.selectedIdArr.filter({$0 == id}).count == 0{
                    let cell:UITableViewCell = self.msgTableView.cellForRow(at: selectedIndexPath ?? IndexPath(row: 0, section: 0))!
                    cell.tag = (selectedIndexPath?.row ?? 0) + 400
                    //forward only downloaded media files
                    cell.backgroundColor = SEPARTOR_COLOR
                    self.forwardView.isHidden = false
                    self.selectedIdArr.append(id)
                    self.selectedIndexArr.append(selectedIndexPath!)
                    
                    self.selectedDict.append(dict)

                    //msg type
                    if msg_type == "text" {
                        self.copyBtn.isHidden = false
                        self.copyIcon.isHidden = false
                    }else{
                        self.copyBtn.isHidden = true
                        self.copyIcon.isHidden = true
                    }
                    self.checkDownloadStatus()
                }
            }else{
                self.forwardView.isHidden = true
                let index = self.selectedIdArr.firstIndex(of: id)
                self.selectedIdArr.remove(at: index ?? 0)
                let selectedIndex = self.selectedIndexArr.firstIndex(of: selectedIndexPath!)
                self.selectedIndexArr.remove(at: selectedIndex ?? 0)
                
                if selectedIndexPath!.count != 0 {
                    let cell = view.viewWithTag(selectedIndexPath!.row + 400) as? UITableViewCell
                    cell?.backgroundColor = .clear
                }
            }
        }
    }
    
    //dismiss keyboard & attachment menu
    @objc func dismissKeyboard() {
        messageTextView.resignFirstResponder()
        self.scrollToBottom()
    }
    
    
    @IBAction func copyBtnTapped(_ sender: Any) {
        let msgDict :NSDictionary = localDB.getMsg(msg_id: self.selectedIdArr.first ?? "")
        UIPasteboard.general.string = msgDict.value(forKeyPath: "message_data.message") as? String
        self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "copied") as? String)
        self.forwardView.isHidden = true
        self.selectedIdArr.removeAll()
        self.selectedDict.removeAll()
        self.selectedIndexArr.removeAll()
        self.scrollTag = 1
        self.msgTableView.reloadData()
    }
    @IBAction func deleteBtnTapped(_ sender: Any) {
        var typeTag = 0
        let own_id:String = UserModel.shared.userID()! as String
        for i in 0..<self.selectedIdArr.count {
            let msgDict =  NSMutableDictionary()
            msgDict.setValue(self.selectedDict[i].message_data, forKey: "message_data")
            let chatTime:String = msgDict.value(forKeyPath: "message_data.chat_time") as! String
            let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            let sender_id:String = self.selectedDict[i].sender_id
            
            let cal = Calendar.current
            let d1 = Date()
            let d2 = Date.init(timeIntervalSince1970: Double(chatTime) ?? 0) // April 27, 2018 12:00:00 AM
            let components = cal.dateComponents([.hour], from: d2, to: d1)
            let diff = components.hour!
            // Print(diff)
            if sender_id != own_id || diff >= 1 || type == "isDelete"{
                typeTag = 0
                break
            }
            else {
                typeTag = 1
            }
        }
        let alert = DeleteAlertViewController()
        alert.modalPresentationStyle = .overCurrentContext
        alert.modalTransitionStyle = .crossDissolve
        alert.delegate = self
        alert.viewType = "0"
        alert.typeTag = typeTag
        alert.msg = "delete_msg"
        self.present(alert, animated: true, completion: nil)
        
    }
    func deleteActionDone(type:String, viewType: String) {
        if type == "0" {
            self.deleteForMeAct()
        }
        else {
            self.deleteForEveryOneAct()
        }
    }
    func deleteForEveryOneAct() {
        self.deleteImageAndVideoFromPhotoLibrary(onSuccess: {response in
            for i in self.selectedDict {
                let msgDict =  NSMutableDictionary()
                let cryptLib = CryptLib()
                let encryptedMsg = cryptLib.encryptPlainTextRandomIV(withPlainText:"This message was Deleted", key: ENCRYPT_KEY)
                msgDict.setValue(encryptedMsg, forKey: "message")
                msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
                msgDict.setValue(i.message_data.value(forKey: "message_id") as? String ?? "", forKey: "message_id")
                msgDict.setValue(self.contact_id, forKey: "receiver_id")
                msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                msgDict.setValue(i.message_data.value(forKey: "chat_time") as? String ?? "", forKey: "chat_time")
                msgDict.setValue("1", forKey: "read_status")
                msgDict.setValue("single", forKey: "chat_type")
                msgDict.setValue("isDelete", forKey: "message_type")
                
                let requestDict = NSMutableDictionary()
                requestDict.setValue(i.sender_id, forKey: "sender_id")
                requestDict.setValue(i.receiver_id, forKey: "receiver_id")
                requestDict.setValue(msgDict, forKey: "message_data")
                socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                self.localDB.updateMessage(message_type: "isDelete", msg_id: i.message_data.value(forKey: "message_id") as? String ?? "")
            }
            self.selectedIndexArr.removeAll()
            self.selectedIdArr.removeAll()
            self.selectedDict.removeAll()
            DispatchQueue.main.async {
                self.forwardView.isHidden = true
                self.refresh(type:"scroll")
                self.scrollToBottom()
            }
        })
    }
    @IBAction func forwardBtnTapped(_ sender: Any) {
        self.messageTextView.resignFirstResponder()
        let forwardObj = ForwardSelection()
        forwardObj.msgID = self.selectedIdArr
        forwardObj.msgFrom = "single"
        forwardObj.delegate = self
        self.navigationController?.pushViewController(forwardObj, animated: true)
    }
    
    func forwardMsg(type: String, msgDict: String) {
        self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "sending") as? String)
        self.forwardView.isHidden = true
        for selectedIndexPath in self.selectedIndexArr {
            if selectedIndexPath.count != 0 {
                let cell = view.viewWithTag(selectedIndexPath.row + 400) as? UITableViewCell
                cell?.backgroundColor = .clear
            }
        }
        self.selectedIdArr.removeAll()
        self.selectedDict.removeAll()
        self.selectedIndexArr.removeAll()
    }
    
    //navigation back btn tapped
    @IBAction func backBtnTapped(_ sender: Any) {
        // audioPlayer.stop()
        if currentAudioMsgID != "" {
            audioPlayer.stop()
            self.timer.invalidate()
        }
        
        if self.selectedIdArr.count != 0  {
            self.forwardView.isHidden = true
            if selectedIndexArr.count != 0 {
                for i in selectedIndexArr {
                    let cell = view.viewWithTag(i.row + 400) as? UITableViewCell
                    cell?.backgroundColor = .clear
                    
                }
                self.selectedIndexArr.removeAll()
            }
            self.selectedIdArr.removeAll()
            self.selectedDict.removeAll()
            self.scrollTag = 0
            self.msgTableView.reloadData()
        }else{
            if self.viewType == "1" {
                self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
            }
            else if self.viewType == "10" {
                self.del.setInitialViewController(initialView: menuContainerPage())
//                UserModel.shared.setTab(index: 0)
            }
            else if self.viewType == "2"{
                self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
            }
            else if self.viewType == "0"{
                self.navigationController?.popViewController(animated: true)
                UserModel.shared.setTab(index: 0)
            }
            else if self.viewType == "3"{
                if let anIndex = navigationController?.viewControllers[1] {
                    navigationController?.popToViewController(anIndex, animated: true)
                    UserModel.shared.setTab(index: 0)
                }
            }
        }
    }
    func stopAudioPlayer() {
        let dict:messageModel.message = msgArray.object(at: tag_value) as! messageModel.message
        let sender_Tag_id:String = dict.sender_id
        let own_id:String = UserModel.shared.userID()! as String
        audioPlayer.currentTime = TimeInterval(0)
        if sender_Tag_id != own_id {
            let cell1 = view.viewWithTag(tag_value + 4000) as? ReceiverVoiceCell
            audioPlayer.stop()
            self.timer.invalidate()
            cell1?.playerImg.image = UIImage(named:"play_audio.png")
            cell1?.audioProgress.value = Float(0)//Float(audioPlayer.currentTime)
            cell1?.PlayerBtn.clipsToBounds = true
        }
        else {
            let cell1 = view.viewWithTag(tag_value + 3000) as? SenderAudioCell
            audioPlayer.stop()
            cell1?.audioProgress.value = Float(0)//Float(audioPlayer.currentTime)
            self.timer.invalidate()
            cell1?.playerImg.image = UIImage(named:"play_audio.png")
            cell1?.PlayerBtn.clipsToBounds = true
            
        }
    }
    @objc func audioCellBtnTapped(_ sender: UIButton!)  {
        if Utility.shared.isConnectedToNetwork(){
            let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
            let msgDict =  NSMutableDictionary()
            msgDict.setValue(dict.message_data, forKey: "message_data")
            // Print(msgDict)
            if self.selectedIdArr.count == 0{
                let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
                if type != "date_sticky" {
                    let message_id:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
                    let sender_id:String = dict.sender_id
                    let own_id:String = UserModel.shared.userID()! as String
                    let updatedDict = self.localDB.getMsg(msg_id: message_id)
                    var videoName:String = updatedDict.value(forKeyPath: "message_data.local_path") as! String
                    if videoName == "0"{
                        let serverLink = msgDict.value(forKeyPath: "message_data.attachment") as! String
                        videoName = "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(serverLink)"
                    }
                    // Print("-------------Audio_URL : %@",videoName)
                    let videoURL = URL.init(string: videoName)
                    let message = videoURL?.lastPathComponent

                    dowloadFile(audioString: videoName,message:message ?? "audio")
                    
                    let isDownload = msgDict.value(forKeyPath: "message_data.isDownload") as! String
                    if tag_value != sender.tag && counter != 0 {
                        stopAudioPlayer()
                    }
                    
                    if sender_id != own_id {
                        //check its downloaded
                        if isDownload == "0" {
                            self.downloadaudiofromserver(index: sender.tag, model: dict)
                        }
                        else if isDownload == "1" || isDownload == "2"
                        {
                            //if !audioPlayer.isPlaying || audioPlayer == nil {
                            if let audioUrl = URL(string: videoName) {
                                // then lets create your document folder url
                                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                // lets create your destination file url
                                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                                
                                do {
                                    
                                    let msg_id = msgDict.value(forKeyPath:"message_data.message_id") as! String
                                    
                                    if currentAudioMsgID.isEmpty || currentAudioMsgID != msg_id{
                                        audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
//                                        audioPlayer.volume = 3
                                        audioPlayer.delegate = self
                                        audioPlayer.prepareToPlay()
                                        currentAudioMsgID = msg_id
                                    }
                                    
                                    
                                    let cell = view.viewWithTag(sender.tag + 4000) as? ReceiverVoiceCell
                                    let maximumvalue:Double = audioPlayer.duration
                                    //duration(for: videoName)
                                    let int_value:Int = Int(maximumvalue)
                                    
                                    
                                    cell?.audioProgress?.minimumValue = 0
                                    cell?.audioProgress?.maximumValue = Float(int_value)
                                    str_value_tofind_which_voiceCell = "ReceiverVoiceCell"
                                    if counter == 0 {
                                        audioPlayer.play()
                                        cell?.playerImg.image = UIImage(named:"pause_receive.png")
                                        let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
//                                        cell?.audioProgress.value = normalizedTime
                                        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateUIWithTimer(_:)), userInfo: [cell?.audioProgress.tag ?? 0], repeats: true)
                                        //cell?.audioProgress.value = Float(audioPlayer.currentTime)
                                        cell?.audioProgress.clipsToBounds = true
                                        cell?.PlayerBtn.clipsToBounds = true
                                        
                                        
                                        tag_value = sender.tag
                                        
                                        counter = 1
                                    } else {
                                        if tag_value == sender.tag {
                                            audioPlayer.stop()
                                            self.timer.invalidate()
                                            
                                            cell?.playerImg.image = UIImage(named:"play_receive.png")
                                            cell?.audioProgress.value = Float(audioPlayer.currentTime)
                                            cell?.PlayerBtn.clipsToBounds = true
                                            counter = 0
                                        }
                                        else {
                                            if (cell?.audioProgress.value ?? 0) != 0 {
                                                let audioValue = cell?.audioProgress.value
                                                audioPlayer.currentTime = Double(audioValue ?? 0) //* audioPlayer.duration / 100 // audioPlayer.currentTime * 100.0 / audioPlayer.duration
                                            }
                                            // Print(audioPlayer.currentTime)
                                            audioPlayer.play()
                                            cell?.playerImg.image = UIImage(named:"pause_receive.png")
                                            //cell?.PlayerBtn.setImage(UIImage(named:"pause_audio.png"), for: UIControlState.normal)
                                            
                                            let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
                                            // Print(normalizedTime)
                                            //                                        cell?.audioProgress.value = normalizedTime
                                            
                                            cell?.PlayerBtn.clipsToBounds = true
                                            tag_value = sender.tag
                                            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateUIWithTimer(_:)), userInfo: [cell?.audioProgress.tag ?? 0], repeats: true)
                                            
                                            counter = 1
                                        }
                                    }
                                } catch let error {
                                    // Print(error.localizedDescription)
                                }
                            }
                            
                        }
                    }else{
                        if isDownload == "0" {
                            
                            if let audioUrl = URL(string: videoName) {
                                
                                // then lets create your document folder url
                                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                
                                // lets create your destination file url
                                let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
                                
                                do {
                                    let msg_id = msgDict.value(forKeyPath:"message_data.message_id") as! String
                                    //                                    if tag_value != sender.tag && counter != 0 {
                                    //                                    }
                                    
                                    if currentAudioMsgID.isEmpty || currentAudioMsgID != msg_id{
                                        audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                                        audioPlayer.delegate = self
                                        audioPlayer.prepareToPlay()
                                        currentAudioMsgID = msg_id
                                    }
                                    // Print(audioPlayer.currentTime)
                                    let maximumvalue:Double = audioPlayer.duration
                                    //duration(for: videoName)
                                    
                                    let int_value:Int = Int(maximumvalue)
                                    
                                    let cell = view.viewWithTag(sender.tag + 3000) as? SenderAudioCell
                                    cell?.audioProgress?.minimumValue = 0
                                    cell?.audioProgress?.maximumValue = Float(int_value)
                                    str_value_tofind_which_voiceCell = "SenderAudioCell"
                                    
                                    if counter == 0 {
                                        if (cell?.audioProgress.value ?? 0) != 0 {
                                            let audioValue = cell?.audioProgress.value
                                            audioPlayer.currentTime = Double(audioValue ?? 0) //* audioPlayer.duration / 100 // audioPlayer.currentTime * 100.0 / audioPlayer.duration
                                        }
                                        audioPlayer.play()
                                        cell?.playerImg.image = UIImage(named:"pause_audio.png")
                                        //cell?.PlayerBtn.setImage(UIImage(named:"pause_audio.png"), for: UIControlState.normal)
                                        
                                        let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
                                        // Print(normalizedTime)
                                        //                                        cell?.audioProgress.value = normalizedTime
                                        
                                        cell?.PlayerBtn.clipsToBounds = true
                                        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateUIWithTimer(_:)), userInfo: [cell?.audioProgress.tag ?? 0], repeats: true)
                                        
                                        tag_value = sender.tag
                                        counter = 1
                                    } else {
                                        if tag_value == sender.tag {
                                            audioPlayer.stop()
                                            self.timer.invalidate()
                                            
                                            cell?.playerImg.image = UIImage(named:"play_audio.png")
                                            cell?.audioProgress.value = Float(audioPlayer.currentTime)
                                            cell?.PlayerBtn.clipsToBounds = true
                                            counter = 0
                                        }
                                        else {
                                            if (cell?.audioProgress.value ?? 0) != 0 {
                                                let audioValue = cell?.audioProgress.value
                                                audioPlayer.currentTime = Double(audioValue ?? 0) //* audioPlayer.duration / 100 // audioPlayer.currentTime * 100.0 / audioPlayer.duration
                                            }
                                            // Print(audioPlayer.currentTime)
                                            audioPlayer.play()
                                            cell?.playerImg.image = UIImage(named:"pause_audio.png")
                                            //cell?.PlayerBtn.setImage(UIImage(named:"pause_audio.png"), for: UIControlState.normal)
                                            
                                            let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
                                            // Print(normalizedTime)
                                            //                                        cell?.audioProgress.value = normalizedTime
                                            
                                            cell?.PlayerBtn.clipsToBounds = true
                                            tag_value = sender.tag
                                            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateUIWithTimer(_:)), userInfo: [cell?.audioProgress.tag ?? 0], repeats: true)
                                            
                                            counter = 1
                                        }
                                    }
                                    
                                } catch let error {
                                    // Print(error.localizedDescription)
                                }
                            }
                            
                            
                        }else if isDownload == "4"{//cancelled
                            
                            if Utility.shared.isConnectedToNetwork(){
                                let msg_id:String = updatedDict.value(forKeyPath: "message_data.message_id") as! String
                                self.localDB.updateDownload(msg_id: msg_id, status: "0")
                                self.scrollTag = 1
                                self.msgTableView.reloadData()
                                PhotoAlbum.sharedInstance.getVideo(local_ID: videoURL!, msg_id: msg_id, requestData: updatedDict,type:(videoURL?.pathExtension)!)
                            } else{
                                self.messageTextView.resignFirstResponder()
                                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
                            }
                        }
                    }
                }
            }else{
                let indexpath = IndexPath.init(row: sender.tag, section: 0)
                self.makeSelection(tag: sender.tag, index: indexpath)
            }
        }
            
        else{
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    func duration(for resource: String) -> Double {
        let asset = AVURLAsset(url: URL(fileURLWithPath: resource))
        return Double(CMTimeGetSeconds(asset.duration))
    }
    
    @objc func updateUIWithTimer(_ timer: Timer){
        if str_value_tofind_which_voiceCell == "SenderAudioCell" {
            //            if info[0] as? Int ?? 0 == tag
            let cell = view.viewWithTag(tag_value + 3000) as? SenderAudioCell
            
            let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
            // Print(normalizedTime)
            if normalizedTime == 0.0 {
                if counter != 0 {
                    counter = 0
                }
                self.timer.invalidate()
                cell?.audioProgress.value = normalizedTime
                let currentTime = Int(audioPlayer.duration)
                let minutes = currentTime/60
                let seconds = currentTime - minutes / 60
                currentAudioMsgID = ""
                self.audioPlayer.stop()
                cell?.audioTimeLbl.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                cell?.playerImg.image = UIImage(named:"play_audio.png")
                
            }else{
                cell?.audioProgress.value = Float(audioPlayer.currentTime)
                let currentTime = Int(audioPlayer.currentTime)
                let minutes = currentTime/60
                let seconds = currentTime - minutes / 60
                cell?.audioTimeLbl.text = NSString(format: "%02d:%02d", minutes,seconds) as String

                if counter != 0 {
                    cell?.playerImg.image = UIImage(named:"pause_audio.png")
                }
            }
            audioPlayer.updateMeters()
            cell?.audioProgress.clipsToBounds = true
            
        }else if str_value_tofind_which_voiceCell == "ReceiverVoiceCell"{
            let cell = view.viewWithTag(tag_value + 4000) as? ReceiverVoiceCell
            let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
            // Print(normalizedTime)
            if normalizedTime == 0.0 {
                self.timer.invalidate()
                cell?.audioProgress.value = normalizedTime
                let currentTime = Int(audioPlayer.currentTime)
                let minutes = currentTime/60
                let seconds = currentTime - minutes / 60
                currentAudioMsgID = ""
                if counter != 0 {
                    counter = 0
                }
                cell?.audioTimeLbl.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                cell?.playerImg.image = UIImage(named:"play_receive.png")
                
                
            }else{
                cell?.audioProgress.value = Float(audioPlayer.currentTime)
                let currentTime = Int(audioPlayer.currentTime)
                let minutes = currentTime/60
                let seconds = currentTime - minutes / 60
                
                cell?.audioTimeLbl.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                if normalizedTime >= 0.0{
                    if counter != 0 {
                        cell?.playerImg.image = UIImage(named:"pause_receive.png")
                    }
                }
                
            }
            audioPlayer.updateMeters()
            cell?.audioProgress.clipsToBounds = true
            
        }
        
    }
    
    //update online status
    @objc func updateOnlineStatus(){
        socketClass.sharedInstance.sendOnlineStatus(contact_id: self.contact_id)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 2.0, options: .curveLinear, animations: {
            if !Utility.shared.isConnectedToNetwork(){
                self.lastSeenLbl.isHidden = true
                //                self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
            }
            /*   let privacy_lastseen:String = self.chatDetailDict.value(forKey: "privacy_lastseen") as! String
             if privacy_lastseen == "nobody"{
             self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
             }*/
        })
    }
    
    //refresh view
    func refresh(type:String){
        isScrollBottom = false
        self.isFetch = false
        let newMsg = self.localDB.getChat(chat_id: self.chat_id, offset: "0")
        self.tempMsgs = self.localDB.getChat(chat_id: self.chat_id, offset: "0")
        self.msgArray = Utility.shared.arrangeMsg(array:newMsg!)
        
        if self.msgArray.count != 0 {
            self.scrollTag = 0
            self.msgTableView.reloadData()
            
        }else{
            self.scrollTag = 0
            //reload to empty table view
            self.msgTableView.reloadData()
        }
        
        if type != "NoScroll" {
            self.scrollToBottom()
        }
    }
    
    @IBAction func callBtnTapped(_ sender: Any)    {
        
        self.messageTextView.resignFirstResponder()
        if Utility.shared.isConnectedToNetwork() {
            if blockByMe == "1"{
                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_call") as? String)
            }else{
                DispatchQueue.main.async {
                    if self.counter != 0 {
                        self.counter = 0
                        self.stopAudioPlayer()
                    }
                    let random_id = Utility.shared.random()
                    let pageobj = CallPage()
                    pageobj.receiverId = self.contact_id
                    pageobj.senderFlag = true
                    pageobj.random_id = random_id
                    pageobj.userdict = self.chatDetailDict
                    pageobj.call_type = "audio"
                    let time = NSDate().timeIntervalSince1970
                    pageobj.modalPresentationStyle = .fullScreen
                    self.localCallDB.addNewCall(call_id: random_id, contact_id: self.contact_id, status: "outgoing", call_type: "audio", timestamp: "\(time.rounded().clean)", unread_count: "0")
                    self.present(pageobj, animated: true, completion: nil)
                }
            }
        }else{
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    @IBAction func videoBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            self.messageTextView.resignFirstResponder()
            if blockByMe == "1"{
                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_call") as? String)
            }else{
                let random_id = Utility.shared.random()
                let pageobj = CallPage()
                pageobj.receiverId = self.contact_id
                pageobj.random_id = random_id
                pageobj.senderFlag = true
                pageobj.call_type = "video"
                pageobj.userdict = self.chatDetailDict
                let time = NSDate().timeIntervalSince1970
                // Print(time.rounded().clean)
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                pageobj.modalPresentationStyle = .fullScreen
                self.localCallDB.addNewCall(call_id: random_id, contact_id: self.contact_id, status: "outgoing", call_type: "video", timestamp: "\(time.rounded().clean)", unread_count: "0")
                self.present(pageobj, animated: true, completion: nil)
            }
        }else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    
    
    @IBAction func profileViewBtnTapped(_ sender: Any) {
        //        UIApplication.shared.statusBarStyle = .lightContent
        self.messageTextView.resignFirstResponder()
        let profileObj = ProfilePage()
        profileObj.viewType = "other"
        profileObj.chatID = self.chat_id
        profileObj.contactName = self.contactNameLbl.text!
        profileObj.contact_id = self.contact_id
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        if viewType == "1" || viewType == "2" {
            profileObj.exitType = self.viewType
            profileObj.modalPresentationStyle = .fullScreen
            self.present(profileObj, animated: true, completion: nil)
        }else if viewType == "0"{
            profileObj.exitType = "0"
            profileObj.modalPresentationStyle = .fullScreen
            self.present(profileObj, animated: true, completion: nil)
        }
    }
    
    @IBAction func menuBtnTapped(_ sender: Any) {
        self.messageTextView.resignFirstResponder()
        let alert = CustomAlert()
        alert.modalPresentationStyle = .overCurrentContext
        alert.modalTransitionStyle = .crossDissolve
        alert.delegate = self
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        let configMenu = FTPopOverMenuConfiguration()
        configMenu.textColor =  TEXT_PRIMARY_COLOR
        configMenu.shadowColor = .white
        configMenu.allowRoundedArrow = true
        configMenu.tintColor = .red
        var frame = self.menuBtn.frame
        if UserModel.shared.getAppLanguage() == "عربى" {
            frame = CGRect(x: self.view.frame.origin.x + 5, y: self.menuBtn.frame.origin.y + self.menuBtn.frame.height + 10, width: self.menuBtn.frame.width, height: self.menuBtn.frame.height)
            alert.alignmentTag = 1
        }
        else {
            frame = CGRect(x: self.view.frame.width - 5, y: self.menuBtn.frame.origin.y + self.menuBtn.frame.height + 10, width: self.menuBtn.frame.width, height: self.menuBtn.frame.height)
            alert.alignmentTag = 1
        }
        
        FTPopOverMenu.show(fromSenderFrame: frame, withMenuArray: self.menuArray as? [Any], doneBlock: { selectedIndex in
            
            if selectedIndex == 0{
                let mute:String = self.chatDetailDict.value(forKey: "mute") as! String
                if mute == "0"{
                    alert.viewType = "3"
                    alert.msg = "mute_msg"
                    self.present(alert, animated: true, completion: nil)
                }else if mute == "1"{
                    alert.viewType = "4"
                    alert.msg = "unmute_msg"
                    self.present(alert, animated: true, completion: nil)
                }
            }else if selectedIndex == 1{
                let type:String = self.menuArray.object(at: 1) as! String
                if type == "Block"{
                    alert.viewType = "0"
                    alert.msg = "block_msg"
                    self.present(alert, animated: true, completion: nil)
                }else if type == "UnBlock"{
                    alert.viewType = "1"
                    alert.msg = "unblock_msg"
                    self.present(alert, animated: true, completion: nil)
                }
            }else if selectedIndex == 2{
                if self.msgArray.count == 0 {
                    self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "no_clear") as? String)
                }
                else {
                    alert.viewType = "2"
                    alert.msg = "clear_msg"
                    self.present(alert, animated: true, completion: nil)
                }
            }else if selectedIndex == 3{
                let contact_name:String = self.chatDetailDict.value(forKey: "contact_name") as! String
                let phone_no:String = self.chatDetailDict.value(forKey: "user_phoneno") as! String
                let cc: String = self.chatDetailDict.value(forKey: "countrycode") as! String

                if contact_name == "+\(cc) \(phone_no)" {
                    self.addAsContact()
                }else{
                    self.addAsFavourite()
                }
            }else if selectedIndex == 4{
                self.addAsFavourite()
            }
        }, dismiss: {
        })
    }
    
    func addAsFavourite()  {
        let fav:String = self.chatDetailDict.value(forKey: "favourite") as! String
        let alert = CustomAlert()
        alert.modalPresentationStyle = .overCurrentContext
        alert.modalTransitionStyle = .crossDissolve
        alert.delegate = self
        if fav == "0" {
            alert.msg = "add_fav"
            alert.viewType = "5"
        }else{
            alert.msg = "remove_fav"
            alert.viewType = "6"
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    //add new contact
    
    func addAsContact()  {
        let phone_no:String = self.chatDetailDict.value(forKey: "user_phoneno") as! String
        let store = CNContactStore()
        let contact = CNMutableContact()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :phone_no))
        contact.phoneNumbers = [homePhone]
        contact.namePrefix = ""
        let controller: CNContactViewController = CNContactViewController(forNewContact: contact)
        controller.contactStore = store
        controller.delegate = self
        let navigationController: UINavigationController = UINavigationController(rootViewController: controller)
//        controller.modalPresentationStyle = .overFullScreen
        self.navigationController?.isNavigationBarHidden = false
        navigationController.modalPresentationStyle = .overFullScreen
        present(navigationController, animated: false) {
            // Print("Present")
        }
//        self.navigationController?.pushViewController(controller, animated: true)
//        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
        if contact?.givenName != nil || contact != nil{
            self.localDB.updateName(cotact_id: self.contact_id, name: (contact?.givenName)!)
            DispatchQueue.global(qos: .background).async {
                Contact.sharedInstance.synchronize()
            }
            self.navigationController?.isNavigationBarHidden = true
            self.dismiss(animated: true, completion: nil)
//            self.navigationController?.popViewController(animated: true)
        }
        else {
            self.navigationController?.isNavigationBarHidden = true
            self.dismiss(animated: true, completion: nil)
//            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
    
    //alert delegate
    func alertActionDone(type: String) {
        self.messageTextView.resignFirstResponder()
        if type == "0"{
            var blockType = String()
            self.blockByMe = "1"
            blockType = "block"
            self.configBlockedStatus()
            self.configFavStatus()
            socketClass.sharedInstance.blockContact(contact_id: self.contact_id, type: blockType)
        }else if type == "1"{
            var blockType = String()
            self.blockByMe = "0"
            blockType = "unblock"
            self.configBlockedStatus()
            self.configFavStatus()
            socketClass.sharedInstance.blockContact(contact_id: self.contact_id, type: blockType)
        }else if type == "2"{
            self.localDB.deleteChat(chat_id: self.chat_id)
            self.tempMsgs?.removeAllObjects()
            self.msgArray.removeAllObjects()
            self.refresh(type:"NoScroll")
        }else if type == "3"{
            self.localDB.updateMute(cotact_id: self.contact_id, status: "1")
            self.configMuteStatus()
        }else if type == "4"{
            self.localDB.updateMute(cotact_id: self.contact_id, status: "0")
            self.configMuteStatus()
        }else if type == "5"{
            self.localDB.updateFavourite(cotact_id: contact_id, status: "1")
            self.configFavStatus()
        }else if type == "6"{
            self.localDB.updateFavourite(cotact_id: contact_id, status: "0")
            self.configFavStatus()
        }else if type == "7"{
            self.deleteForMeAct()
        }
    }
    func deleteImageAndVideoFromPhotoLibrary(onSuccess success: @escaping (Bool) -> Void) {
        var localPathArr = [String]()
        for i in selectedDict {
            let messageID = i.message_data.value(forKey: "message_id") as? String ?? ""
            let localMsg = localDB.getMsg(msg_id: messageID)
            let localPath = localMsg.value(forKeyPath: "message_data.local_path") as! String
            let senderID = i.sender_id
            let deleteType = i.message_data.value(forKey: "message_type") as? String ?? ""
            if (deleteType == "image" || deleteType == "video") && senderID != (UserModel.shared.userID() as String? ?? "") {
                localPathArr.append(localPath)
            }
        }
        PhotoAlbum.sharedInstance.delete(local_ID: localPathArr, onSuccess: {response in
            success(response)
        })
    }
    func deleteForMeAct() {
        var selectedIDVal = ""
        for id in 0..<self.selectedIdArr.count {
            if id == 0 {
                selectedIDVal = "'" + self.selectedIdArr[id] + "'"
            }
            else {
                selectedIDVal = selectedIDVal + ",'" + self.selectedIdArr[id] + "'"
            }
        }
        self.deleteImageAndVideoFromPhotoLibrary(onSuccess: {response in
            if response {
                DispatchQueue.main.async {
                    self.localDB.deleteMsg(msg_id: selectedIDVal)
                    self.forwardView.isHidden = true
                    self.refresh(type:"scroll")
                    self.updateLastMsg()
                    self.selectedIndexArr.removeAll()
                    self.selectedIdArr.removeAll()
                    self.selectedDict.removeAll()
                }
            }
        })
        
    }
    func updateLastMsg() {
        let time = NSDate().timeIntervalSince1970

        if self.msgArray.count > 0 {
            let lastMsg = self.msgArray.object(at: self.msgArray.count - 1) as! messageModel.message
            if lastMsg.message_data.value(forKey: "message_type") as? String == "date_sticky" {
                if self.msgArray.count > 1 {
                    let lastMsg1 = self.msgArray.object(at: self.msgArray.count - 2) as! messageModel.message
                    self.localDB.updateRecentMessage(chat_id: self.chat_id, message_id: lastMsg1.message_data.value(forKey: "message_id") as? String ?? "You Deleted this message", time: lastMsg1.message_data.value(forKey: "chat_time") as? String ?? time.clean)
                }
            }
            else {
                self.localDB.updateRecentMessage(chat_id: self.chat_id, message_id: lastMsg.message_data.value(forKey: "message_id") as? String ?? "You Deleted this message", time: lastMsg.message_data.value(forKey: "chat_time") as? String ?? time.clean)
            }
        }
        else {
            self.localDB.updateRecentMessage(chat_id: self.chat_id, message_id: "You Deleted this message", time: "\(time)")
        }
    }
    // getsture delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.msgTableView) == true {
            return false
        }
        return true
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    //text msg send btn
    @IBAction func sendBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            if !Utility.shared.checkEmptyWithString(value: messageTextView.text) {
                // prepare socket  dict
                let msgDict = NSMutableDictionary()
                let msg_id = Utility.shared.random()
                let msg = messageTextView.text.trimmingCharacters(in: .newlines)
                let cryptLib = CryptLib()
                let encryptedMsg = cryptLib.encryptPlainTextRandomIV(withPlainText:msg, key: ENCRYPT_KEY)
                let time = NSDate().timeIntervalSince1970
                //        let key = "123"
                //        let cryptLib = CryptLib()
                //        let cipherText = cryptLib.encryptPlainTextRandomIV(withPlainText: msg, key: key)
                msgDict.setValue(encryptedMsg, forKey: "message")
                
                msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
                msgDict.setValue(msg_id, forKey: "message_id")
                msgDict.setValue("text", forKey: "message_type")
                msgDict.setValue(self.contact_id, forKey: "receiver_id")
                msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                msgDict.setValue("1", forKey: "read_status")
                msgDict.setValue("single", forKey: "chat_type")
                
                let requestDict = NSMutableDictionary()
                requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                requestDict.setValue(self.contact_id, forKey: "receiver_id")
                requestDict.setValue(msgDict, forKey: "message_data")
                //send socket
                print("blockByMe: \(blockByMe), blockedMe: \(blockedMe)")
                if blockByMe == "0" && blockedMe == "0"{
                    socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                    self.addToLocal(requestDict: requestDict)
                }else if blockByMe == "1"{
                    self.messageTextView.resignFirstResponder()
                    self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
                }else{
                    self.addToLocal(requestDict: requestDict)
                }
                self.configSendBtn(enable: false)
                self.ConfigVoiceBtn(enable: true)

                self.messageTextView.text = EMPTY_STRING
            }
        }else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    // add to local db
    func addToLocal(requestDict:NSDictionary)  {
        // Print("LOCAL DICT \(requestDict)")
        var lat : String = EMPTY_STRING
        var lon : String = EMPTY_STRING
        var cName : String = EMPTY_STRING
        let cc : String = EMPTY_STRING
        var cNo : String = EMPTY_STRING
        var attach : String = EMPTY_STRING
        var thumbnail : String = EMPTY_STRING
        
        let type : String = requestDict.value(forKeyPath: "message_data.message_type") as! String
        if type == "image"{
            attach = requestDict.value(forKeyPath: "message_data.attachment") as! String
        }else if type == "location"{
            lat = requestDict.value(forKeyPath: "message_data.lat") as! String
            lon = requestDict.value(forKeyPath: "message_data.lon") as! String
        }else if type == "contact"{
            //            cc = requestDict.value(forKeyPath: "message_data.cc") as! String
            cName = requestDict.value(forKeyPath: "message_data.contact_name") as! String
            cNo = requestDict.value(forKeyPath: "message_data.contact_phone_no") as! String
        }else if type == "video"{
            attach = requestDict.value(forKeyPath: "message_data.attachment") as! String
            thumbnail = requestDict.value(forKeyPath: "message_data.thumbnail") as! String
        }else if type == "document"{
            attach = requestDict.value(forKeyPath: "message_data.attachment") as! String
        }
        else if type == "audio"{
            attach = requestDict.value(forKeyPath: "message_data.attachment") as! String
        }
        
        //add local db
        let msgDict = localDB.getMsg(msg_id: requestDict.value(forKeyPath: "message_data.message_id") as! String)
        var readCount =  String()
        if msgDict.value(forKeyPath: "message_data.read_status") == nil{
            readCount = "1"
        }else{
            readCount = msgDict.value(forKeyPath: "message_data.read_status") as! String
        }
        self.localDB.addChat(msg_id: requestDict.value(forKeyPath: "message_data.message_id") as! String,
                             chat_id: self.chat_id,
                             sender_id:requestDict.value(forKey: "sender_id")! as! String,
                             receiver_id:requestDict.value(forKey: "receiver_id")! as! String,
                             msg_type: requestDict.value(forKeyPath: "message_data.message_type") as! String,
                             msg: requestDict.value(forKeyPath: "message_data.message") as! String,
                             time: requestDict.value(forKeyPath: "message_data.chat_time") as! String,
                             lat: lat,
                             lon: lon,
                             contact_name: cName,
                             contact_no: cNo,
                             country_code: cc,
                             attachment: attach,thumbnail:thumbnail, read_count: readCount, statusData: "")
        if msgDict.value(forKey: "local_path") != nil {
            
        }
        let unreadcount = localDB.getUnreadCount(contact_id: self.contact_id)
        self.localDB.addRecent(contact_id: self.contact_id, msg_id: requestDict.value(forKeyPath: "message_data.message_id") as! String, unread_count: "\(unreadcount)",time: requestDict.value(forKeyPath: "message_data.chat_time") as! String)
        
        //add local array
        isScrollBottom = false
        self.isFetch = false
        self.tempMsgs?.removeAllObjects()
        let newMsg = self.localDB.getChat(chat_id: self.chat_id, offset: "0")
        self.tempMsgs = self.localDB.getChat(chat_id: self.chat_id, offset: "0")
        self.msgArray.removeAllObjects()
        self.msgArray = Utility.shared.arrangeMsg(array:newMsg!)
        self.scrollTag = 0
        self.msgTableView.reloadData()
        
        //        self.viewDidLayoutSubviews()
        if Utility.shared.checkEmptyWithString(value: messageTextView.text) {
            self.configSendBtn(enable: false)
            self.ConfigVoiceBtn(enable:true)
        }else{
            self.configSendBtn(enable: true)
            self.ConfigVoiceBtn(enable:false)
        }
    }
    
    //open attachment menu
    @IBAction func attachmentMenuBtnTapped(_ sender: Any) {
        self.messageTextView.resignFirstResponder()
        if self.attachmentShow{
            self.showAttachmentMenu(enable: false)
        }else{
            self.showAttachmentMenu(enable: true)
        }
    }
    
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        return self.msgArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let msgDict =  NSMutableDictionary()
        let dict:messageModel.message = self.msgArray.object(at: indexPath.row) as! messageModel.message
        msgDict.setValue(dict.message_data, forKey: "message_data")
        let type:String = msgDict.value(forKeyPath: "message_data.message_type") as? String ?? ""
        
        if type == "text" || type == "isDelete"{ // type text
            return UITableView.automaticDimension
        }else if type == "image"{ // type image
            return 150
        }else if type == "location"{ // type location
            return 150
        }else if type == "audio"{
            return 70
        }else if type == "contact"{ // type contact
            if dict.sender_id == (UserModel.shared.userID() as String? ?? ""){
                return 95
            }
            else {
                return 125
            }
        }else if type == "video"{ // type video
            return 150
        }else if type == "document"{ // type document
            return 75
        }else if type == "date_sticky"{
            return 40
        }else if type == "audio"{
            return 70
        }
        else if type == "story" {
            return UITableView.automaticDimension
        }
        return 45
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.msgArray.count == 0 {
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChatDetailsSectionTableViewCell") as? ChatDetailsSectionTableViewCell
            return cell
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.msgArray.count == 0 {
            return UITableView.automaticDimension
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customcell = UITableViewCell()
        let dict:messageModel.message = self.msgArray.object(at: indexPath.row) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
        let sender_id:String = dict.sender_id
        let own_id:String = UserModel.shared.userID()! as String
        if type == "text" || type == "isDelete"{ // type text
            let CellIdentifier = "TextCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! TextCell
            cell.bgView.isUserInteractionEnabled = true
            cell.sender_id = sender_id
            cell.nameLabel.isHidden = true
            cell.own_id = own_id
            var deleteStatus = CGFloat(0)
            cell.deleteIcon.tintColor = .white
            if type == "isDelete" {
                cell.deleteIcon.isHidden = false
                deleteStatus = 20.0
                if sender_id == own_id {
                    cell.deleteIcon.tintColor = TEXT_PRIMARY_COLOR
                    msgDict.setValue("deleted_by_you", forKey: "message_data.message")
                }
                else {
                    cell.deleteIcon.tintColor = .white
                    msgDict.setValue("deleted_by_others", forKey: "message_data.message")
                }
            }
            else {
                deleteStatus = 0
                cell.deleteIcon.isHidden = true
            }
            cell.config(msgDict: msgDict)
            let msg:String = msgDict.value(forKeyPath: "message_data.message") as? String ?? ""
//            print(msg)
            let val = heightForView(text: msg, font: UIFont.init(name:APP_FONT_REGULAR, size: 16)!, isDelete: deleteStatus)
//            if sender_id == own_id {
//                cell.stackViewHeight.constant = 45 + val.height
//            }
//            else {
//                cell.stackViewHeight.constant = 45 + val.height
//            }
//            cell.MsgTextView.sizeToFit()
            cell.labelWidth.constant = val.width
            customcell = cell
            customcell.backgroundColor = .clear
            //            }
        }else if type == "image"{ // type image
            if sender_id == own_id{
                let senderImgCell = tableView.dequeueReusableCell(withIdentifier: "SenderImageCell", for: indexPath) as! SenderImageCell
                senderImgCell.config(msgDict: msgDict)
                senderImgCell.tag = indexPath.row+50000
                senderImgCell.imageBtn.tag = indexPath.row
                senderImgCell.imageBtn.addTarget(self, action: #selector(self.imageCellBtnTapped), for: .touchUpInside)
                customcell = senderImgCell
            }else{
                let receiverImgCell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageCell", for: indexPath) as! ReceiverImageCell
                receiverImgCell.config(msgDict: msgDict)
                receiverImgCell.tag = indexPath.row+50000
                receiverImgCell.imageBtn.tag = indexPath.row
                receiverImgCell.imageBtn.addTarget(self, action: #selector(self.imageCellBtnTapped), for: .touchUpInside)
                customcell = receiverImgCell
            }
            //            tableView.rowHeight = 150
            
        }
        else if type == "audio"{
            if sender_id == own_id{
                let updatedDict = self.localDB.getMsg(msg_id: msgDict.value(forKeyPath:"message_data.message_id") as! String)
                let senderImgCell = tableView.dequeueReusableCell(withIdentifier: "SenderAudioCell", for: indexPath) as! SenderAudioCell
                senderImgCell.selectionStyle = .none
                senderImgCell.PlayerBtn.tag = indexPath.row
                senderImgCell.tag = indexPath.row+3000
                //senderImgCell.audioProgress.addTarget(self, action: #selector(self.trackaudio), for: ([.touchUpInside,.touchUpOutside]))
                // senderImgCell.audioProgress.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
                senderImgCell.audioProgress.tag = indexPath.row
                senderImgCell.audioProgress.addTarget(self, action: #selector(respondToSlideEvents), for: .valueChanged)
                senderImgCell.PlayerBtn.addTarget(self, action: #selector(self.audioCellBtnTapped), for: .touchUpInside)
                senderImgCell.config(msgDict: updatedDict)
                
                
                customcell = senderImgCell
            }else{
                
                let receiverImgCell = tableView.dequeueReusableCell(withIdentifier: "ReceiverVoiceCell", for: indexPath) as! ReceiverVoiceCell
                receiverImgCell.selectionStyle = .none
                receiverImgCell.config(msgDict: msgDict)
                receiverImgCell.tag = indexPath.row+4000
                receiverImgCell.PlayerBtn.tag = indexPath.row
                receiverImgCell.audioProgress.tag = indexPath.row

                //receiverImgCell.audioProgress.addTarget(self, action: #selector(self.trackaudio), for: ([.touchUpInside,.touchUpOutside]))
                //receiverImgCell.audioProgress.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
                receiverImgCell.audioProgress.addTarget(self, action: #selector(respondToSlideEvents), for: .valueChanged)
                
                receiverImgCell.PlayerBtn.addTarget(self, action: #selector(self.audioCellBtnTapped), for: .touchUpInside)
                customcell = receiverImgCell
            }
        }
        else if type == "location"{ // type location
            if sender_id == own_id{
                let senderLocObj = tableView.dequeueReusableCell(withIdentifier: "SenderLocCell", for: indexPath) as! SenderLocCell
                senderLocObj.config(msgDict: msgDict)
                senderLocObj.locationBtn.tag = indexPath.row
                senderLocObj.locationBtn.addTarget(self, action: #selector(self.locationTapped), for: .touchUpInside)
                customcell = senderLocObj
            }else{
                let receiverLocObj = tableView.dequeueReusableCell(withIdentifier: "ReceiverLocCell", for: indexPath) as! ReceiverLocCell
                receiverLocObj.config(msgDict: msgDict)
                receiverLocObj.locationBtn.tag = indexPath.row
                receiverLocObj.locationBtn.addTarget(self, action: #selector(self.locationTapped), for: .touchUpInside)
                customcell = receiverLocObj
            }
            
            //            tableView.rowHeight = 150
        }else if type == "contact"{ // type contact
            if sender_id == own_id{
                let senderContactObj = tableView.dequeueReusableCell(withIdentifier: "SenderContact", for: indexPath) as! SenderContact
                senderContactObj.config(msgDict: msgDict)
                customcell = senderContactObj
                //                tableView.rowHeight = 95
            }else{
                let receiverContactObj = tableView.dequeueReusableCell(withIdentifier: "ReceiverContact", for: indexPath) as! ReceiverContact
                receiverContactObj.config(msgDict: msgDict)
                receiverContactObj.contactAddBtn.tag = indexPath.row
                receiverContactObj.contactAddBtn.addTarget(self, action: #selector(self.addToContact), for: .touchUpInside)
                customcell = receiverContactObj
                //                tableView.rowHeight = 125
            }
        }else if type == "video"{ // type video
            if sender_id == own_id{
                let updatedDict = self.localDB.getMsg(msg_id: msgDict.value(forKeyPath:"message_data.message_id") as! String)
                let sendVideo = tableView.dequeueReusableCell(withIdentifier: "SenderVideoCell", for: indexPath) as! SenderVideoCell
                sendVideo.videoBtn.tag = indexPath.row
                sendVideo.videoBtn.addTarget(self, action: #selector(self.videoCellBtnTapped), for: .touchUpInside)
                sendVideo.config(msgDict: updatedDict)
                customcell = sendVideo
            }else{
                let receiveVideo = tableView.dequeueReusableCell(withIdentifier: "ReceiverVideoCell", for: indexPath) as! ReceiverVideoCell
                receiveVideo.config(msgDict: msgDict)
                receiveVideo.tag = indexPath.row+20000
                receiveVideo.videoBtn.tag = indexPath.row
                receiveVideo.videoBtn.addTarget(self, action: #selector(self.videoCellBtnTapped), for: .touchUpInside)
                customcell = receiveVideo
            }
            //            tableView.rowHeight = 150
            
        }else if type == "document"{ // type document
            if sender_id == own_id{
                let sendDoc = tableView.dequeueReusableCell(withIdentifier: "SenderDocuCell", for: indexPath) as! SenderDocuCell
                sendDoc.config(msgDict: msgDict)
                sendDoc.docBtn.tag = indexPath.row
                sendDoc.docBtn.addTarget(self, action: #selector(self.docuCellBtnTapped), for: .touchUpInside)
                customcell = sendDoc
            }else{
                let receiveDocu = tableView.dequeueReusableCell(withIdentifier: "ReceiverDocuCell", for: indexPath) as! ReceiverDocuCell
                receiveDocu.config(msgDict: msgDict)
                receiveDocu.tag = indexPath.row
                receiveDocu.docBtn.tag = indexPath.row
                receiveDocu.docBtn.addTarget(self, action: #selector(self.docuCellBtnTapped), for: .touchUpInside)
                customcell = receiveDocu
            }
            //            tableView.rowHeight = 75
            
        }else if type == "date_sticky"{
            let dateSticky = tableView.dequeueReusableCell(withIdentifier: "dateStickyCell", for: indexPath) as! dateStickyCell
            let dateString = msgDict.value(forKeyPath: "message_data.date") as? String ?? ""
            let dateformat =  DateFormatter()
            
            dateformat.locale = Locale(identifier: "en_US")
            dateformat.dateFormat = "dd MMM yyyy"
            let dateNew = dateformat.string(from: Date())
            if dateNew == dateString  {
                dateSticky.dateLbl.attributedText = NSAttributedString.init(string: Utility.shared.getLanguage()?.value(forKey: "today") as? String ?? "Today")
            }else{
                dateSticky.dateLbl.attributedText = NSAttributedString.init(string: dateString)
            }
            if UserModel.shared.getAppLanguage() == "عربى" {
                dateSticky.dateLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            else {
                dateSticky.dateLbl.transform = .identity
            }
            customcell = dateSticky
            //            tableView.rowHeight = 40
        }
        else if type == "story" {
            let storyCell = tableView.dequeueReusableCell(withIdentifier: "StatusReplyTableViewCell", for: indexPath) as! StatusReplyTableViewCell
            storyCell.replyMessageLabel.text = "hii"
            storyCell.wholeBackgroundView.isUserInteractionEnabled = true
            storyCell.sender_id = sender_id
            storyCell.receiveUserNAmeLabel.isHidden = true
            storyCell.own_id = own_id
            storyCell.wholeStatusView.tag = indexPath.row
            storyCell.wholeStatusView.isUserInteractionEnabled = true
            storyCell.wholeStatusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageGestureAct(_:))))
            storyCell.receiveUserNAmeLabel.isHidden = true
            if storyCell.sender_id != storyCell.own_id {
                storyCell.statusTitleLabel.text = (Utility.shared.getLanguage()?.value(forKey: "your_story") as? String ?? "Your Story") + " " + (Utility.shared.getLanguage()?.value(forKey: "status") as? String ?? "Status")
            }
            else {
                storyCell.statusTitleLabel.text = self.contactNameLbl.text! + " " + (Utility.shared.getLanguage()?.value(forKey: "status") as? String ?? "Status")
            }
            storyCell.config(msgDict: msgDict)
//            storyCell.replyMessageLabel.sizeToFit()
            customcell = storyCell
        }
        if type != "date_sticky"{
            if self.selectedDict.contains(where: {$0.message_data == dict.message_data}){
                customcell.backgroundColor = SEPARTOR_COLOR
            }else{
                customcell.backgroundColor = .clear
            }
        }
        //        customcell.tag = indexPath.row
        if indexPath.row == (self.msgArray.count - 1) && self.scrollTag == 0 {
            self.scrollToBottom()
        }
        return customcell
        
    }
    func jsonToString(value: String) -> Dictionary<String, Any>? {
        let string = value
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
                // Print(jsonArray) // use the json here
                return jsonArray
            } else {
                // Print("bad json")
            }
        } catch let error as NSError {
            // Print(error)
        }
        return nil
    }
    @objc func imageGestureAct(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag
        let storage = storyStorage()
        let dict:messageModel.message = self.msgArray.object(at: tag ?? 0) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        let jsonString:String = msgDict.value(forKeyPath: "message_data.status_data") as? String ?? ""
        let cryptLib = CryptLib()
        let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: jsonString, key: ENCRYPT_KEY)
        
        if let value = self.jsonToString(value: decryptedMsg ?? "") {
            print(value)
            let id:String = value["story_id"] as? String ?? ""
            let stryData = storage.checkIfExsit(story_id: id).first
            if stryData != nil {
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                let vc = ContentViewController()
                vc.modalPresentationStyle = .overFullScreen
                let sender_id:String = dict.sender_id
                let own_id:String = UserModel.shared.userID()! as String
                var userStatus = RecentStoryModel(sender_id: stryData?.sender_id ?? "", story_id: stryData?.story_id ?? "", message: "", story_type: "", attachment: "", story_date: "", story_time: "", expiry_time: "", contactName: "", userName: "", phoneNumber: "", userImage: "", aboutUs: "", blockedMe: "", blockedByMe: "", mute: "", mutual_status: "", privacy_lastseen: "", privacy_about: "", privacy_image: "", favourite: "")
                if sender_id != own_id {
                    userStatus = RecentStoryModel(sender_id: stryData?.sender_id ?? "", story_id: stryData?.story_id ?? "", message: "", story_type: "", attachment: "", story_date: "", story_time: "", expiry_time: "", contactName: "", userName: "", phoneNumber: "", userImage: "", aboutUs: "", blockedMe: "", blockedByMe: "", mute: "", mutual_status: "", privacy_lastseen: "", privacy_about: "", privacy_image: "", favourite: "")
                }
                else {
                    userStatus = RecentStoryModel(sender_id: stryData?.sender_id ?? "", story_id: stryData?.story_id ?? "", message: "", story_type: "", attachment: "", story_date: "", story_time: "", expiry_time: "", contactName: self.contactNameLbl.text!, userName: "", phoneNumber: "", userImage: "", aboutUs: "", blockedMe: "", blockedByMe: "", mute: "", mutual_status: "", privacy_lastseen: "", privacy_about: "", privacy_image: "", favourite: "")
                }
                vc.pages = [userStatus]
                print(userStatus)
                vc.currentIndex = 0
                vc.isFromChat = true
                let statusDict = storage.getUserInfo(userID: stryData?.sender_id ?? "")
                let index = statusDict.firstIndex(where: {$0.story_id == stryData?.story_id})
                print(index)
                vc.segIndex = index ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.messageTextView.resignFirstResponder()
        let dict:messageModel.message = msgArray.object(at: indexPath.row) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        let type:String = msgDict.value(forKeyPath: "message_data.message_type") as? String ?? ""
        if self.selectedIdArr.count != 0 && type != "date_sticky"{
            self.makeSelection(tag: indexPath.row, index: indexPath)
        }
    }
    @objc func respondToSlideEvents(sender: UISlider) {
        let currentValue: Float = Float(sender.value)
        // Print("Event fired. Current value for slider: \(currentValue)%.")
        if self.tag_value == sender.tag {
            if str_value_tofind_which_voiceCell == "SenderAudioCell" {
                audioPlayer.currentTime = TimeInterval(currentValue)
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateUIWithTimer(_:)), userInfo:[sender.tag], repeats:true)
            }
            else if str_value_tofind_which_voiceCell == "ReceiverVoiceCell"{
                audioPlayer.currentTime = TimeInterval(currentValue)
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateUIWithTimer(_:)), userInfo:[sender.tag], repeats:true)
            }
        }
    }
    
    func makeSelection(tag:Int,index:IndexPath)  {
        let dict:messageModel.message = msgArray.object(at: tag) as! messageModel.message
        let msg_type = dict.message_data.value(forKey: "message_type") as! String
        //        if selectedIndexPath.count != 0 {
        //            let cell = view.viewWithTag(selectedIndexPath.row + 400) as? UITableViewCell
        //            cell?.backgroundColor = .clear
        //        }
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        if msg_type != "date_sticky"{
            let id = dict.message_data.value(forKey: "message_id") as! String
            if self.selectedIdArr.filter({$0 == id}).count == 0 {
                let cell:UITableViewCell = self.msgTableView.cellForRow(at: index)!
                cell.tag = tag + 400
                self.selectedIndexArr.append(index)
                cell.backgroundColor = SEPARTOR_COLOR
                self.selectedIdArr.append(id)
                self.selectedDict.append(dict)
                self.forwardView.isHidden = false
            }else{
                if self.selectedIdArr.filter({$0 == id}).count != 0 {
                    let cell = view.viewWithTag(index.row + 400) as? UITableViewCell
                    cell?.backgroundColor = .clear
                }
                let id = self.selectedIdArr.firstIndex(of: id)
                if id != nil {
                    self.selectedIdArr.remove(at: id ?? 0)
                    self.selectedDict.remove(at: id ?? 0)
                    let selectedIndex = self.selectedIndexArr.firstIndex(of: index)
                    self.selectedIndexArr.remove(at: selectedIndex ?? 0)
                }
            }
            if self.selectedDict.count != 1 {
                self.copyBtn.isHidden = true
                self.copyIcon.isHidden = true
            }
            else if msg_type == "text"{
                self.copyBtn.isHidden = false
                self.copyIcon.isHidden = false
            }
            else {
                self.copyBtn.isHidden = true
                self.copyIcon.isHidden = true
            }
            self.checkDownloadStatus()
        }else{
            for index in self.selectedIndexArr {
                if index.count != 0 {
                    let cell = view.viewWithTag(index.row + 400) as? UITableViewCell
                    cell?.backgroundColor = .clear
                }
            }
        }
        if self.selectedIdArr.count == 0 {
            self.forwardView.isHidden = true
            if self.messageTextView.becomeFirstResponder() {
                dismissKeyboard()
            }
        }
    }
    func checkDownloadStatus() {
        for dict in self.selectedDict {
            let downloadStatus = dict.message_data.value(forKey: "isDownload") as? String ?? ""
            let message_type = dict.message_data.value(forKey: "message_type") as! String
            print(message_type)
            if (message_type == "image" && message_type == "video" && message_type == "audio") && (downloadStatus == "0") && (dict.sender_id != UserModel.shared.userID() as String? ?? ""){
                self.forwardIconView.isHidden = true
                break
            }
            else {
                if message_type == "isDelete" {
                   self.forwardIconView.isHidden = true
                    break
                }
                self.forwardIconView.isHidden = false
            }
        }
    }
    @objc func docuCellBtnTapped(_ sender: UIButton!)  {
        self.messageTextView.resignFirstResponder()
        let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        if self.selectedIdArr.count == 0 {
            let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            if type != "date_sticky" {
                if type == "document"{
                    let message_id:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
                    let sender_id:String = dict.sender_id
                    let own_id:String = UserModel.shared.userID()! as String
                    
                    
                    let updatedDict = self.localDB.getMsg(msg_id: message_id)
                    var docName:String = updatedDict.value(forKeyPath: "message_data.local_path") as! String
                    if docName == "0"{
                        let serverLink = msgDict.value(forKeyPath: "message_data.attachment") as! String
                        docName = "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(serverLink)"
                    }
                    // Print("-------------Audio_URL : %@",videoName)
                    _ = URL.init(string: docName)
                    let message = msgDict.value(forKeyPath: "message_data.message") as! String
                    dowloadFile(audioString: docName, message: message)
                    let isDownload = msgDict.value(forKeyPath: "message_data.isDownload") as! String
                    
                    if sender_id != own_id{
                        //check its downloaded
                        if isDownload == "0" {
                            self.downloadDocument(index: sender.tag, model: dict)
                        }else if isDownload == "1"{
                            DispatchQueue.main.async {
                                let docuName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
                                // Print("\(docuName)")
                                if self.counter != 0 {
                                    self.counter = 0
                                    self.stopAudioPlayer()
                                }
                                let webVC = SwiftModalWebVC(urlString: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(docuName)")
                                webVC.modalPresentationStyle = .fullScreen
                                self.present(webVC, animated: true, completion: nil)
                            }
                        }
                    }else{
                        //check if uploaded or not
                        if isDownload == "1" {
                            DispatchQueue.main.async {
                                let docuName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
                                // Print("\(docuName)")
                                if self.counter != 0 {
                                    self.counter = 0
                                    self.stopAudioPlayer()
                                }
                                let webVC = SwiftModalWebVC(urlString: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(docuName)")
                                webVC.modalPresentationStyle = .fullScreen
                                self.present(webVC, animated: true, completion: nil)
                            }
                        }else if isDownload == "4"{//cancelled
                            if Utility.shared.isConnectedToNetwork(){
                                DispatchQueue.main.async {
                                    let docuName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
                                    // Print("\(docuName)")
                                    if self.counter != 0 {
                                        self.counter = 0
                                        self.stopAudioPlayer()
                                    }
                                    let webVC = SwiftModalWebVC(urlString: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(docuName)")
                                    webVC.modalPresentationStyle = .fullScreen
                                    self.present(webVC, animated: true, completion: nil)
                                }
                            } else{
                                self.messageTextView.resignFirstResponder()
                                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                let docuName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
                                // Print("\(docuName)")
                                let webVC = SwiftModalWebVC(urlString: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(docuName)")
                                webVC.modalPresentationStyle = .fullScreen
                                self.present(webVC, animated: true, completion: nil)
                            }
                        }
                    }
                    
                }
            }
        }else{
            let indexpath = IndexPath.init(row: sender.tag, section: 0)
            self.makeSelection(tag: sender.tag, index: indexpath)
        }
    }
    
    @objc func imageCellBtnTapped(_ sender: UIButton!)  {
        let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        if self.selectedIdArr.count == 0 {
            let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            if type != "date_sticky" {
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                let message_id:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
                let sender_id:String = dict.sender_id
                let own_id:String = UserModel.shared.userID()! as String
                
                DispatchQueue.main.async {
                    let updatedDict = self.localDB.getMsg(msg_id: message_id)
                    let local_path:String = updatedDict.value(forKeyPath: "message_data.local_path") as! String
                    if sender_id != own_id{
                        let isDownload:String = msgDict.value(forKeyPath: "message_data.isDownload") as! String
                        if isDownload == "0" {
                            self.downloadImage(index: sender.tag, model:dict)
                        }else{
                            self.openPic(identifier: local_path,msgDict:updatedDict)
                        }
                    }else{
                        self.openPic(identifier: local_path,msgDict:updatedDict)
                    }
                }
                
            }
        }else{
            let indexpath = IndexPath.init(row: sender.tag, section: 0)
            self.makeSelection(tag: sender.tag, index: indexpath)
        }
    }
    
    @objc func videoCellBtnTapped(_ sender: UIButton!)  {
        let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        if self.selectedIdArr.count == 0 {
            let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            if type != "date_sticky" {
                let message_id:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
                let sender_id:String = dict.sender_id
                let own_id:String = UserModel.shared.userID()! as String
                
                
                let updatedDict = self.localDB.getMsg(msg_id: message_id)
                var videoName:String = updatedDict.value(forKeyPath: "message_data.local_path") as! String
                if videoName == "0"{
                    let serverLink = msgDict.value(forKeyPath: "message_data.attachment") as! String
                    videoName = "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(serverLink)"
                }
                let videoURL = URL.init(string: videoName)
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                let isDownload:String = msgDict.value(forKeyPath: "message_data.isDownload") as! String
                if sender_id != own_id{
                    //check its downloaded
                    if isDownload == "0" {
                        self.downloadVideo(index: sender.tag, model: dict)
                    }else if isDownload == "1"{
                        if self.counter != 0 {
                            self.counter = 0
                            self.stopAudioPlayer()
                        }
                        playerViewController.modalPresentationStyle = .fullScreen
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }
                }else{
                    //check if uploaded or not
                    if isDownload == "1" {
                        if self.counter != 0 {
                            self.counter = 0
                            self.stopAudioPlayer()
                        }
                        playerViewController.modalPresentationStyle = .fullScreen
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }else if isDownload == "4"{//cancelled
                        if Utility.shared.isConnectedToNetwork(){
                            let msg_id:String = updatedDict.value(forKeyPath: "message_data.message_id") as! String
                            self.localDB.updateDownload(msg_id: msg_id, status: "0")
                            self.scrollTag = 1
                            self.msgTableView.reloadData()
                            PhotoAlbum.sharedInstance.getVideo(local_ID: videoURL!, msg_id: msg_id, requestData: updatedDict,type:(videoURL?.pathExtension)!)
                        } else{
                            self.messageTextView.resignFirstResponder()
                            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
                        }
                    }
                }
            }
        }else{
            let indexpath = IndexPath.init(row: sender.tag, section: 0)
            self.makeSelection(tag: sender.tag, index: indexpath)
        }
    }
    
    @objc func locationTapped(_ sender: UIButton!)  {
        let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        if self.selectedIdArr.count == 0 {
            let type:String = msgDict.value(forKeyPath: "message_data.message_type") as! String
            if type != "date_sticky" {
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                let locationObj = PickLocation()
                locationObj.type = "1"
                locationObj.locationDict = msgDict
                locationObj.modalPresentationStyle = .fullScreen
                self.navigationController?.present(locationObj, animated: true, completion: nil)
            }
        }else{
            let indexpath = IndexPath.init(row: sender.tag, section: 0)
            self.makeSelection(tag: sender.tag, index: indexpath)
        }
    }
    
    @objc func addToContact(_ sender: UIButton!)  {
        let dict:messageModel.message = msgArray.object(at: sender.tag) as! messageModel.message
        let msgDict =  NSMutableDictionary()
        msgDict.setValue(dict.message_data, forKey: "message_data")
        if #available(iOS 9.0, *) {
            let phoneNo:String = msgDict.value(forKeyPath: "message_data.cNo") as! String
            let name:String = msgDict.value(forKeyPath: "message_data.cName") as! String
            let store = CNContactStore()
            let contact = CNMutableContact()
            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :phoneNo))
            contact.phoneNumbers = [homePhone]
            contact.namePrefix = name
            let controller = CNContactViewController(forUnknownContact : contact)
            controller.contactStore = store
            controller.delegate = self
            controller.modalPresentationStyle = .fullScreen
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController!.pushViewController(controller, animated: true)
        }
    }
    
    //move to gallery
    func openPic(identifier:String,msgDict:NSDictionary){
        DispatchQueue.main.async {
            if identifier != "0" {
                let galleryPic = PhotoAlbum.sharedInstance.getImage(local_ID: identifier)
                if galleryPic == nil{
                    self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "item_not_found") as? String)
                }else{
                    if self.counter != 0 {
                        self.counter = 0
                        self.stopAudioPlayer()
                    }
                    let imageInfo = GSImageInfo.init(image: galleryPic!, imageMode: .aspectFit)
                    let transitionInfo = GSTransitionInfo(fromView: self.view)
                    let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
                    imageViewer.modalPresentationStyle = .fullScreen
                    self.present(imageViewer, animated: true, completion: nil)
                }
            }else{
                let imageName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
                let imageURL = URL.init(string: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)")
                let data = try? Data(contentsOf: imageURL!)
                var image =  UIImage()
                if let imageData = data {
                    image = UIImage(data: imageData) ?? #imageLiteral(resourceName: "profile_placeholder")
                }
                let imageInfo = GSImageInfo.init(image: image, imageMode: .aspectFit, imageHD: nil)
                let transitionInfo = GSTransitionInfo(fromView: self.view)
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
                imageViewer.modalPresentationStyle = .fullScreen
                self.present(imageViewer, animated: true, completion: nil)
            }
        }
    }
    
    func getDataFromAssetAtUrl( assetUrl: URL, success: @escaping (_ data: NSData) -> ()){
        let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [assetUrl], options: nil)
        if let phAsset = fetchResult.firstObject {
            PHImageManager.default().requestImageData(for: phAsset, options: nil) {
                (imageData, dataURI, orientation, info) -> Void in
                if let imageDataExists = imageData {
                    success(imageDataExists as NSData)
                }
            }
        }
    }
    
    //load more action
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = self.msgTableView.contentOffset
        if currentOffset.y < 50{
            if isFetch == false {
                isFetch = true
                //get new msg from service based on offset
                var previousMsg:AnyObject?
                previousMsg = self.localDB.getChat(chat_id: self.chat_id, offset: "\((tempMsgs?.count)!)")
                if previousMsg?.count != 0{
                    //prepare added final array to load
                    self.finalArray.removeAllObjects()
                    self.finalArray.addObjects(from: previousMsg as! [Any])
                    self.finalArray.addObjects(from:tempMsgs as! [Any])
                    self.msgArray.removeAllObjects()
                    self.msgArray.addObjects(from: Utility.shared.arrangeMsg(array:finalArray) as! [Any])
                    // add over all temp array
                    self.tempMsgs?.removeAllObjects()
                    self.tempMsgs?.addObjects(from: self.finalArray as! [Any])
                    DispatchQueue.main.async {
                        if self.msgArray.count != 0{
                            self.msgTableView.reloadData()
                            //                    let indexPath = IndexPath(row: (previousMsg?.count)!-1, section: 0)
                            //                    self.msgTableView.scrollToRow(at: indexPath, at: .top, animated: false)
                            //                    self.isFetch = false
                        }
                    }
                }
            }
        }else{
            if !isScrollBottom{
                viewDidLayoutSubviews()
                isScrollBottom = true
            }
        }
    }
    
    //DOWNLOAD IMAGE
    func downloadImage(index:Int, model :messageModel.message)  {
        let msgDict = NSMutableDictionary()
        msgDict.setValue(model.message_data, forKey: "message_data")
        let messageID:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
        let oldMsgDict = NSMutableDictionary.init(dictionary: model.message_data)
        self.localDB.updateDownload(msg_id: messageID, status: "2")
        let cell = view.viewWithTag(index + 50000) as? ReceiverImageCell
        cell?.loader.play()
        cell?.loader.isHidden = false
        cell?.downloadIcon.isHidden = true
        cell?.downloadView.isHidden = true
        let imageName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
        DispatchQueue.global(qos: .background).async {
            let imageURL = URL(string:"\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)")
            let data = try? Data(contentsOf: imageURL!)
            if let imageData = data {
                let image = UIImage(data: imageData)
                let messageID:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
                PhotoAlbum.sharedInstance.save(image: image!, msg_id: messageID,type:"single")
                self.localDB.updateDownload(msg_id: messageID, status: "1")
                
                oldMsgDict.removeObject(forKey: "isDownload")
                oldMsgDict.setValue("1", forKey: "isDownload")
                let newModel = messageModel.message.init(sender_id: model.sender_id, receiver_id: model.receiver_id, message_data:oldMsgDict, date: model.date)
                self.msgArray.removeObject(at: index)
                self.msgArray.insert(newModel, at: index)
                self.scrollTag = 1
                DispatchQueue.main.async {
                    self.msgTableView.reloadData()
                }
            }
        }
    }
    //DOWNLOAD VIDEO
    func downloadVideo(index:Int, model :messageModel.message)  {
        
        let msgDict = NSMutableDictionary()
        msgDict.setValue(model.message_data, forKey: "message_data")
        let messageID:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
        let oldMsgDict = NSMutableDictionary.init(dictionary: model.message_data)
        
        self.localDB.updateDownload(msg_id: messageID, status: "2")
        let cell = view.viewWithTag(index + 20000) as? ReceiverVideoCell
        cell?.loader.play()
        cell?.loader.isHidden = false
        cell?.downloadIcon.isHidden = true
        cell?.playImgView.image =  #imageLiteral(resourceName: "download_icon")
        
        let videoName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(videoName)"),
                let urlData = NSData(contentsOf: url)
            {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/\(videoName)"
                // Print("file path \(filePath)")
                //                DispatchQueue.main.async {
                
                urlData.write(toFile: filePath, atomically: true)
                PhotoAlbum.sharedInstance.saveVideo(url: URL.init(string: filePath)!, msg_id: messageID, type: "single")
                self.localDB.updateDownload(msg_id: messageID, status: "1")
                oldMsgDict.removeObject(forKey: "isDownload")
                oldMsgDict.setValue("1", forKey: "isDownload")
                let newModel = messageModel.message.init(sender_id: model.sender_id, receiver_id: model.receiver_id, message_data:oldMsgDict, date: model.date)
                self.msgArray.removeObject(at: index)
                self.msgArray.insert(newModel, at: index)
                DispatchQueue.main.async {
                    self.scrollTag = 1
                    self.msgTableView.reloadData()
                }
                //                }
            }
        }
    }
    //DOWNLOAD VIDEO
    func downloadDocument(index:Int, model :messageModel.message)  {
        
        let msgDict = NSMutableDictionary()
        msgDict.setValue(model.message_data, forKey: "message_data")
        let messageID:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
        let oldMsgDict = NSMutableDictionary.init(dictionary: model.message_data)
        
        self.localDB.updateDownload(msg_id: messageID, status: "2")
        let cell = view.viewWithTag(index) as? ReceiverDocuCell
        cell?.loader.play()
        cell?.loader.isHidden = false
        cell?.downloadIcon.isHidden = true
        
        let videoName:String = msgDict.value(forKeyPath: "message_data.message") as! String
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(videoName)"),
                let urlData = NSData(contentsOf: url)
            {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/\(videoName)"
                // Print("file path \(filePath)")
                //                DispatchQueue.main.async {
                
                urlData.write(toFile: filePath, atomically: true)
                self.localDB.updateDownload(msg_id: messageID, status: "1")
                oldMsgDict.removeObject(forKey: "isDownload")
                oldMsgDict.setValue("1", forKey: "isDownload")
                let newModel = messageModel.message.init(sender_id: model.sender_id, receiver_id: model.receiver_id, message_data:oldMsgDict, date: model.date)
                self.msgArray.removeObject(at: index)
                self.msgArray.insert(newModel, at: index)
                DispatchQueue.main.async {
                    self.scrollTag = 1
                    self.msgTableView.reloadData()
                }
                //                }
            }
        }
    }
    
    
    
    //MARK: textview delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        // Print("called")
        self.attachmentMenuView.isHidden = true
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        self.ConfigVoiceBtn(enable:true)
        self.configSendBtn(enable:false)
    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//
//    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let requestDict = NSMutableDictionary()
        let sender_id:String = UserModel.shared.userID()! as String
        let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String
        requestDict.setValue(sender_id, forKey: "sender_id")
        requestDict.setValue(receiver_id, forKey: "receiver_id")
        requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
        requestDict.setValue("typing", forKey: "type")
        if self.blockedMe != "1" && blockByMe == "0"{
            socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
        }
        
        let timeStamp = Date()
        self.timeStamp = timeStamp
        let END_TYPING_TIME: CGFloat = 1.5
        perform(#selector(self.endTyping(_:)), with: timeStamp, afterDelay: TimeInterval(END_TYPING_TIME))
        if Utility.shared.checkEmptyWithString(value: textView.text!+text) {
            self.configSendBtn(enable: false)
            self.ConfigVoiceBtn(enable:true)
            if text == "\n" {
                textView.resignFirstResponder()
                return false
            }
            
        }else{
            if let char = text.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) && (textView.text.count == 1) {
                    self.configSendBtn(enable: false)
                    self.ConfigVoiceBtn(enable: true)
                    return true
                }
            }
            self.configSendBtn(enable: true)
            self.ConfigVoiceBtn(enable:false)

        }
        self.adjustContentSize(tv: textView)
        
        return true
    }
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        self.bootomInputView.frame.origin.y += self.bootomInputView.frame.size.height
        self.bootomInputView.frame.size.height = height+20
        self.messageTextView.frame.size.height = height
        self.bootomInputView.frame.origin.y -= self.bootomInputView.frame.size.height
        self.messageTextView.frame.origin.y = 10
        //        self.msgTableView.frame.size.height -= 60
        //        self.msgTableView.frame.size.height = self.bootomInputView.frame.origin.y - self.navigationView.frame.size.height
        //        self.scrollToBottom()
    }
    
    func adjustContentSize(tv: UITextView){
        let deadSpace = tv.bounds.size.height - tv.contentSize.height
        let inset = max(0, deadSpace/2.0)
        tv.contentInset = UIEdgeInsets(top: inset, left: tv.contentInset.left, bottom: inset, right: tv.contentInset.right)
    }
    
    
    //show & hide attachment menu view
    func showAttachmentMenu(enable:Bool)  {
        if !enable{
            self.attachmentShow = false
            self.attachmentMenuView.isHidden = false
            
        }else{
            //open
            self.attachmentShow = true
            self.attachmentMenuView.isHidden = true
            //            self.msgTableView.frame.size.height -= 60
            //            self.bootomInputView.frame.origin.y -= 60
            //            self.attachmentMenuView.frame.origin.y = FULL_HEIGHT-60
        }
    }
    //MARK: Keyboard hide/show
    @objc func keyboardWillShow(sender: NSNotification) {
        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        //        self.showAttachmentMenu(enable: false)
        self.attachmentShow = true
        self.bottomConst.constant = keyboardFrame.height
        print("Keyboard OverLap \(self.bottomConst.constant)")
        if msgArray.count != 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.chatCount = self.msgArray.count
                let indexPath = IndexPath(row: self.msgArray.count-1, section: 0)
                self.msgTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.bottomConst.constant = 0
        if msgArray.count != 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.chatCount = self.msgArray.count
                self.scrollToBottom()
            }
        }
        if Utility.shared.checkEmptyWithString(value: messageTextView.text) {
            self.configSendBtn(enable: false)
//            self.ConfigVoiceBtn(enable: true)
        }else{
            self.configSendBtn(enable: true)
//            self.ConfigVoiceBtn(enable:false)
        }
    }
    
    @objc func endTyping(_ timeStamp: Date?) {
        if (timeStamp == self.timeStamp) {
            let requestDict = NSMutableDictionary()
            requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
            requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
            let sender_id:String = UserModel.shared.userID()! as String
            let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String

            requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
            requestDict.setValue("untyping", forKey: "type")
            if self.blockedMe != "1" && blockByMe == "0"{
                socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
            }
            self.startTyping = false
            
        }
        
    }
    
    func configSendBtn(enable:Bool)  {
        if enable {
            self.sendBtn.isUserInteractionEnabled = true
            self.sendView.isHidden = false
        }else{
            self.sendBtn.isUserInteractionEnabled = false
            self.sendView.isHidden = true
        }
        
    }
    
    func ConfigVoiceBtn(enable:Bool)
    {
        if(enable){
            self.record_btn_ref.isUserInteractionEnabled = true
            self.record_btn_ref.isHidden = false
            
        }
        else {
            self.record_btn_ref.isUserInteractionEnabled = false
            self.record_btn_ref.isHidden = true
            
        }
    }
    
    
    
    //MARK: ***************** LOCATION PICKER METHODS *********************
    
    @IBAction func locationBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            let locationObj = PickLocation()
            locationObj.delegate = self
            if self.counter != 0 {
                self.counter = 0
                self.stopAudioPlayer()
            }
            locationObj.modalPresentationStyle = .fullScreen
            self.navigationController?.present(locationObj, animated: true, completion: nil)
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    //MARK: location fetch delegate
    func fetchCurrentLocation(location: CLLocation) {
        
        let msgDict = NSMutableDictionary()
        let msg_id = Utility.shared.random()
        let time = NSDate().timeIntervalSince1970
        
        msgDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
        msgDict.setValue(msg_id, forKey: "message_id")
        msgDict.setValue("1", forKey: "read_status")
        msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
        msgDict.setValue("\(location.coordinate.latitude)", forKey: "lat")
        msgDict.setValue("\(location.coordinate.longitude)", forKey: "lon")
        msgDict.setValue(self.contact_id, forKey: "receiver_id")
        msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
        msgDict.setValue("single", forKey: "chat_type")
        
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
        
        msgDict.setValue("location", forKey: "message_type")
        msgDict.setValue("Location", forKey: "message")
        requestDict.setValue(msgDict, forKey: "message_data")
        if blockByMe == "0" && blockedMe == "0"{
            socketClass.sharedInstance.sendMsg(requestDict: requestDict)
            self.addToLocal(requestDict: requestDict)
        }else if blockByMe == "1"{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
        }else{
            self.addToLocal(requestDict: requestDict)
        }
        
        
    }
    //MARK: ***************** DOCUMENT PICKER METHODS *********************
    
    @IBAction func fileBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            if self.counter != 0 {
                self.counter = 0
                self.stopAudioPlayer()
            }
            picDocument()
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    //MARK: pic document from docment drobox icloud
    func picDocument()  {
        let types: NSArray = NSArray.init(objects: kUTTypePDF,kUTTypeText, kUTTypeItem)
        let documentPicker = UIDocumentPickerViewController(documentTypes: types as! [String], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .fullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    
    //MARK: Document picker delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        // you get from the urls parameter the urls from the files selected
        let fileData = NSData.init(contentsOf: URL.init(string: "\(url)")!)
        let fileName = url.lastPathComponent
        let extensionType = ".\(url.pathExtension)"
        self.fileUpload(name: fileName, docuData: fileData!, type: extensionType)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // you get from the urls parameter the urls from the files selected
        let fileData = NSData.init(contentsOf: URL.init(string: "\(urls[0])")!)
        let fileName = urls[0].lastPathComponent
        let extensionType = ".\(urls[0].pathExtension)"
        self.fileUpload(name: fileName, docuData: fileData!, type: extensionType)
    }
    
    func fileUpload(name:String,docuData:NSData,type:String)  {
        let msgDict = NSMutableDictionary()
        let msg_id = Utility.shared.random()
        msgDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
        msgDict.setValue(msg_id, forKey: "message_id")
        msgDict.setValue("1", forKey: "read_status")
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
        msgDict.setValue("document", forKey: "message_type")
        msgDict.setValue(name, forKey: "message")
        self.uploadFiles(msgDict: msgDict, requestDict: requestDict, attachData: docuData as Data, type:type , image: nil)
    }
    
    //MARK: ***************** CONTACT PICKER METHODS *********************
    @IBAction func contactBtnTapped(_ sender: Any) {
        
        if Utility.shared.isConnectedToNetwork() {
            requestForAccess { (accessGranted) in
                if accessGranted == true{
                    self.contactPicker.modalPresentationStyle = .fullScreen
                    self.present(self.contactPicker,animated: true, completion: nil)
                }
            }
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
        
    }
    // Ask contact access permisssion
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async{
                            self.contactPermissionAlert()
                        }
                    }
                }
            })
        default:
            completionHandler(false)
        }
    }
    //MARK:contact restriction alert
    func contactPermissionAlert()  {
        AJAlertController.initialization().showAlert(aStrMessage: "contact_permission", aCancelBtnTitle: "cancel", aOtherBtnTitle: "settings", completion: { (index, title) in
            if index == 1{
                //open settings page
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        })
    }
    
    // MARK: contact picker view delegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        if contact.isKeyAvailable(CNContactPhoneNumbersKey){
            
            if contact.phoneNumbers.count != 0  {
                // handle the selected contact
                let msgDict = NSMutableDictionary()
                let msg_id = Utility.shared.random()
                let time = NSDate().timeIntervalSince1970
                
                msgDict.setValue(UserModel.shared.userID(), forKey: "user_id")
                msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
                msgDict.setValue(msg_id, forKey: "message_id")
                msgDict.setValue("1", forKey: "read_status")
                msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                msgDict.setValue(contact.givenName, forKey: "contact_name")
                msgDict.setValue((contact.phoneNumbers[0].value).value(forKey: "digits") as? String, forKey: "contact_phone_no")
                msgDict.setValue(self.contact_id, forKey: "receiver_id")
                msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                msgDict.setValue("single", forKey: "chat_type")
                let requestDict = NSMutableDictionary()
                requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
                
                msgDict.setValue("contact", forKey: "message_type")
                msgDict.setValue("Contact", forKey: "message")
                requestDict.setValue(msgDict, forKey: "message_data")
                if blockByMe == "0" && blockedMe == "0"{
                    socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                    self.addToLocal(requestDict: requestDict)
                }else if blockByMe == "1"{
                    self.messageTextView.resignFirstResponder()
                    self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
                }else{
                    self.addToLocal(requestDict: requestDict)
                }
            }else{
                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "no_number") as? String)
            }
        }
        
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        // Print("Cancelled picking a contact")
        
    }
    //MARK: ***************** IMAGE/VIDEO PICKER METHODS *********************
    //MARK: Attachment actions
    @IBAction func cameraBtnTapped(_ sender: Any) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            self.openCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.openCamera()
                } else {
                    //access denied
                    DispatchQueue.main.async{
                        self.cameraPermissionAlert()
                    }
                }
            })
        }
        
    }
    func heightForView(text:String, font:UIFont, isDelete: CGFloat) -> CGRect {
        let width = (self.view.frame.width * 0.8) - isDelete
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame
    }
    func openCamera()  {
        //access allowed
        if Utility.shared.isConnectedToNetwork() {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.galleryType = "1"
                let imagePicker = UIImagePickerController()
                //                imagePicker.mediaTypes = ["public.image", "public.movie"]
                imagePicker.mediaTypes = ["public.image"]
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
//                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
                imagePicker.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Cance", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    //MARK:contact restriction alert
    func cameraPermissionAlert()  {
        AJAlertController.initialization().showAlert(aStrMessage: "camera_permission", aCancelBtnTitle: "cancel", aOtherBtnTitle: "settings", completion: { (index, title) in
            if index == 1{
                //open settings page
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        })
    }
    
    @IBAction func galleryBtnTapped(_ sender: Any) {
        if Utility.shared.isConnectedToNetwork() {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.galleryType = "2"
                let imagePicker = UIImagePickerController()
                imagePicker.mediaTypes = ["public.image", "public.movie"]
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                if self.counter != 0 {
                    self.counter = 0
                    self.stopAudioPlayer()
                }
                imagePicker.modalPresentationStyle = .fullScreen
//                imagePicker.videoExportPreset = AVAssetExportPresetPassthrough
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let msgDict = NSMutableDictionary()
        let msg_id = Utility.shared.random()
        msgDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
        msgDict.setValue(msg_id, forKey: "message_id")
        msgDict.setValue("1", forKey: "read_status")
        
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
        
        var attachData = Data()
        var type =  String()
        if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
            if mediaType  == "public.image" {
                let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                // Print("infor \(info)")
                // Print("image orientation \(image.imageOrientation.rawValue)")
                
                if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] {
                    let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL as! URL], options: nil)
                    msgDict.setValue(result.firstObject?.localIdentifier, forKey: "local_path")
                }
                
                attachData = image.jpegData(compressionQuality: 0.5)!//UIImageJPEGRepresentation(image, 0.5)!
                if galleryType == "1"{
                    type = ".jpg"
                }else{
                    let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as! NSURL
                    if (assetPath.absoluteString?.hasSuffix("JPG"))! {
                        type = ".jpg"
                    } else if (assetPath.absoluteString?.hasSuffix("PNG"))! {
                        type = ".png"
                    }
                }
                msgDict.setValue("image", forKey: "message_type")
                msgDict.setValue("Image", forKey: "message")
                self.uploadFiles(msgDict: msgDict, requestDict: requestDict, attachData: attachData, type: type, image: image)
                
            }
            // ********VIDEO PICKER**********
            if mediaType == "public.movie" {
                if let fileURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL {
                    if (fileURL.absoluteString?.hasSuffix("MOV"))! {
                        type = ".mov"
                    } else {
                        type = ".mp4"
                    }
                    
                    let videoData = NSData.init(contentsOf: fileURL as URL)
                    let size = Float((videoData?.length)!) / 1024.0 / 1024.0
                    if size.rounded() < 50 {
                        msgDict.setValue("video", forKey: "message_type")
                        msgDict.setValue("Video", forKey: "message")
                        
                        msgDict.setValue("\((info[UIImagePickerController.InfoKey.referenceURL])!)", forKey: "local_path")
                        self.uploadThumbnail(msgDict: msgDict, requestDict: requestDict, attachData: videoData!, fileURL: fileURL, type: type)
                    }else{
                        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: Utility.shared.getLanguage()?.value(forKey: "file_size") as! String, completion: { (index, title) in
                            self.dismiss(animated:true, completion: nil)
                        })
                    }
                }
            }
        }
    }
    //MARK: image picker delegate
    
    //upload video thumbnail
    func uploadThumbnail(msgDict: NSDictionary, requestDict: NSDictionary, attachData: NSData,fileURL:NSURL,type:String)  {
        if Utility.shared.isConnectedToNetwork() {
            let image = Utility.shared.thumbnailForVideoAtURL(url: fileURL as URL)
//            let flippedImage = UIImage(cgImage: (image?.cgImage)!, scale: (image?.scale)!, orientation: .right)
            let thumbData =  image?.jpegData(compressionQuality: 0.5)//UIImageJPEGRepresentation(flippedImage, 0.5)!
            let uploadObj = UploadServices()
            //upload thumbnail
            uploadObj.uploadFiles(fileData: thumbData!, type: ".jpg", user_id: UserModel.shared.userID()! as String,docuName:msgDict.value(forKey: "message") as! String, msg_id: msgDict.value(forKey: "message_id") as! String,api_type:"private", onSuccess: {response in
                let status:String = response.value(forKey: "status") as! String
                if status == STATUS_TRUE{
                    msgDict.setValue(response.value(forKey: "user_image"), forKey: "thumbnail")
                    let time = NSDate().timeIntervalSince1970
                    msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                    msgDict.setValue(self.contact_id, forKey: "receiver_id")
                    msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                    msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                    msgDict.setValue(EMPTY_STRING, forKey: "attachment")
                    msgDict.setValue("single", forKey: "chat_type")
                    msgDict.setValue("0", forKey: "isDownload")
                    requestDict.setValue(msgDict, forKey: "message_data")
                    if self.blockByMe == "1"{
                        self.messageTextView.resignFirstResponder()
                        self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
                    }else{
                        //upload video file
                        self.addToLocal(requestDict: requestDict)
                        
                        let videoName:String = fileURL.lastPathComponent!
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                        let filePath="\(documentsPath)/\(videoName)"
                        attachData.write(toFile: filePath, atomically: true)
                        if self.galleryType == "1"{ // SAVE VIDEO TO GALLERY
                            PhotoAlbum.sharedInstance.saveVideo(url: URL.init(string: filePath)!, msg_id: msgDict.value(forKey: "message_id") as! String, type: "single")
                        }else{
                            self.localDB.updateLocalURL(msg_id: msgDict.value(forKey: "message_id") as! String, url:msgDict.value(forKey: "local_path") as! String)
                        }
                        socketClass.sharedInstance.uploadChatVideo(fileData: attachData as Data, type: type, msg_id: msgDict.value(forKey: "message_id") as! String, requestDict: requestDict)
                    }
                }
            })
            dismiss(animated:true, completion: nil)
        }else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    
    //upload files
    func uploadFiles(msgDict:NSDictionary,requestDict:NSDictionary,attachData:Data,type:String,image:UIImage?){
        if Utility.shared.isConnectedToNetwork() {
            let uploadObj = UploadServices()
            uploadObj.uploadFiles(fileData: attachData, type: type, user_id: UserModel.shared.userID()! as String,docuName:msgDict.value(forKey: "message") as! String,msg_id: msgDict.value(forKey: "message_id") as! String,api_type:"private", onSuccess: {response in
                let status:String = response.value(forKey: "status") as! String
                if status == STATUS_TRUE{
                    let time = NSDate().timeIntervalSince1970
                    msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                    
                    let cryptLib = CryptLib()
                    let encryptedMsg = response.value(forKey: "user_image") as? String ?? ""// cryptLib.encryptPlainTextRandomIV(withPlainText:response.value(forKey: "user_image") as? String ?? "", key: ENCRYPT_KEY)
                    msgDict.setValue(encryptedMsg, forKey: "attachment")
                    msgDict.setValue(self.contact_id, forKey: "receiver_id")
                    msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                    msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                    msgDict.setValue("0", forKey: "isDownload")
                    msgDict.setValue("single", forKey: "chat_type")
                    
                    requestDict.setValue(msgDict, forKey: "message_data")
                    //send socket
                    if self.blockByMe == "0" && self.blockedMe == "0"{
                        socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                        self.addToLocal(requestDict: requestDict)
                    }else if self.blockByMe == "1"{
                        self.messageTextView.resignFirstResponder()
                        self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
                    }else{
                        self.addToLocal(requestDict: requestDict)
                    }
                    
                    //check if photo is already exists in gallery
                    let msgType:String = msgDict.value(forKey: "message_type") as! String
                    if msgType == "image"{
                        if msgDict.value(forKey: "local_path") != nil{
                            if !PhotoAlbum.sharedInstance.checkExist(identifier: msgDict.value(forKey: "local_path") as! String)!{
                                PhotoAlbum.sharedInstance.save(image: image!, msg_id: msgDict.value(forKey: "message_id") as! String, type: "single")
                            }else{
                                self.localDB.updateLocalURL(msg_id: msgDict.value(forKey: "message_id") as! String, url: msgDict.value(forKey: "local_path") as! String)
                            }
                        }else{
                            PhotoAlbum.sharedInstance.save(image: image!, msg_id: msgDict.value(forKey: "message_id") as! String, type: "single")
                        }
                    }
                }
            })
            dismiss(animated:true, completion: nil)
        } else{
            self.messageTextView.resignFirstResponder()
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    
    //MARK: ********* SOCKET RESPONSE ********
    func gotSocketInfo(dict: NSDictionary, type: String) {
        // Print("CHAT PAGE TYPE \(type) INFO \(dict)")
        if type == "receivechat" {
            let sender_id:String = dict.value(forKey: "sender_id") as! String
            if sender_id == self.contact_id{
                self.refresh(type:"scroll")
                //sent chat read status
                if UIApplication.shared.applicationState != .background || UIApplication.shared.applicationState != .inactive{
                    socketClass.sharedInstance.chatRead(sender_id: dict.value(forKey: "sender_id")! as! String, receiver_id: dict.value(forKey: "receiver_id")! as! String)
                }
                self.localDB.updateRecent(chat_id: self.chat_id)
            }else{
                let userDict = localDB.getContact(contact_id: sender_id)
                /*   let imageName:String = userDict.value(forKey: "user_image") as! String
                 let url = URL.init(string: "\(IMAGE_BASE_URL)\(USERS_SUB_URL)\(imageName)")
                 let data = NSData.init(contentsOf: url!)
                 let image = UIImage(data : data! as Data)*/
                let cryptLib = CryptLib()
                let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: dict.value(forKeyPath: "message_data.message") as? String, key: ENCRYPT_KEY)
//                localNotify.show(withImage:nil , title: userDict.value(forKey: "contact_name") as? String, message: decryptedMsg, onTap: {
//                })
            }
        }else if type == "listentyping"{
            let type:String = dict.value(forKey: "type") as! String
            let sender_id:String = dict.value(forKey: "sender_id") as! String
            if sender_id == self.contact_id{
                if type == "untyping"{
                    self.lastSeenLbl.text = "Online"
                }
                else if type == "typing"{
                    self.lastSeenLbl.text = "typing..."
                }
                else if type == "recording"{
                    self.lastSeenLbl.text = "recording..."
                }
            }
        }else if type == "endchat"{
            let receiver_id:String = dict.value(forKey: "receiver_id") as! String
            if receiver_id == self.contact_id{
                self.refresh(type:"NoScroll")
            }
        }else if type == "viewchat"{
            let receiver_id:String = dict.value(forKey: "receiver_id") as! String
            if receiver_id == self.contact_id{
                self.refresh(type:"NoScroll")
            }
        } else if type == "onlinestatus"{
            let status:String = dict.value(forKey: "livestatus") as! String
            if status == "online"{
                self.lastSeenLbl.isHidden = false
                self.lastSeenLbl.text = "Online"
                if self.blockedMe == "1" || self.blockByMe == "1"{
                    self.lastSeenLbl.isHidden = true
                    //                    self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
                }else{
                    //                    self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y, width: 150, height: 25)
                }
            }else if status == "offline"{
                let lastSeen:NSNumber = dict.value(forKey: "lastseen") as! NSNumber
                self.lastSeenLbl.text = Utility.shared.setStatus(timeStamp: "\(lastSeen)")
                if self.blockedMe == "1" || self.blockByMe == "1"{
                    self.lastSeenLbl.isHidden = true
                    //                    self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
                }else{
                    //last seen
                    let mutual:String = self.chatDetailDict.value(forKey: "mutual_status") as! String
                    let privacy_lastseen:String = self.chatDetailDict.value(forKey: "privacy_lastseen") as! String
                    if privacy_lastseen == "nobody"{
                        self.lastSeenLbl.isHidden = true
                        //                        self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
                    }else if privacy_lastseen == "everyone"{
                        self.lastSeenLbl.isHidden = false
                        //                        self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y, width: 150, height: 25)
                        
                    }else if privacy_lastseen == "mycontacts"{
                        if mutual == "true"{
                            self.lastSeenLbl.isHidden = false
                            //                            self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y , width: 150, height: 25)
                        }else{
                            self.lastSeenLbl.isHidden = true
                            //                            self.contactNameLbl.frame =  CGRect.init(x: self.profilePic.frame.origin.x+50, y:  self.profilePic.frame.origin.y + 10, width: 150, height: 25)
                        }
                    }
                }
            }
        }else if type == "changeuserimage"{
            self.chatDetailDict = localDB.getContact(contact_id: self.contact_id)
            let imageName:String = chatDetailDict.value(forKey: "user_image") as! String
            let urlString:String = "\(IMAGE_BASE_URL)\(USERS_SUB_URL)\(imageName)"
            DispatchQueue.main.async {
                self.profilePic.sd_setImage(with: URL(string:urlString), placeholderImage:  #imageLiteral(resourceName: "profile_placeholder"))
            }
        }else if type == "blockstatus"{
            let blockType = dict.value(forKey: "type") as! String
            let sender_id:String = dict.value(forKey: "sender_id") as! String
            if sender_id == self.contact_id{
                if blockType == "block"{
                    self.blockedMe = "1"
                }else if blockType == "unblock"{
                    self.blockedMe = "0"
                }
                self.configBlockedStatus()
            }
        }else if type == "checkCurrentChat"{
            let sender_id:String = dict.value(forKey: "sender_id") as! String
            if sender_id == self.contact_id{
                if UIApplication.shared.applicationState != .background || UIApplication.shared.applicationState != .inactive{
                    socketClass.sharedInstance.chatRead(sender_id: dict.value(forKey: "sender_id")! as! String, receiver_id: dict.value(forKey: "receiver_id")! as! String)
                }
                self.localDB.readStatus(id: self.chat_id, status: "3", type: "chat")
                self.localDB.updateRecent(chat_id: self.chat_id)
                self.refresh(type:"scroll")
            }
        }else if type == "videoUploadStatus"{
            let receiver_id:String = dict.value(forKey: "receiver_id") as! String
            if receiver_id == self.contact_id{
                self.refresh(type:"NoScroll")
            }
        } else if type == "makeprivate"{
            let user_id:String = dict.value(forKey: "user_id") as! String
            if user_id == self.contact_id{
                self.configPrivacySettings()
            }
        }
    }
    @objc func voiceSent()
    {
        if Utility.shared.isConnectedToNetwork() {
            
            // prepare socket  dict
            let msgDict = NSMutableDictionary()
            let time = NSDate().timeIntervalSince1970
            let key = "123"
            let cryptLib = CryptLib()
            // let cipherText = cryptLib.encryptPlainTextRandomIV(withPlainText: msg, key: key)
            let filename = "myRecording.m4a"
            let filePath = getDocumentsDirectory().appendingPathComponent(filename)
            //let url = URL(string: "URLSTRING HERE")
            let anyvar =  String(describing: filePath)
            let msg_id = Utility.shared.random()
            
            msgDict.setValue(getFileUrl().path, forKey: "message")
            msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
            msgDict.setValue(msg_id, forKey: "message_id")
            msgDict.setValue("audio", forKey: "message_type")
            msgDict.setValue(self.contact_id, forKey: "receiver_id")
            msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
            msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
            msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
            msgDict.setValue("1", forKey: "read_status")
            msgDict.setValue("single", forKey: "chat_type")
            
            let requestDict = NSMutableDictionary()
            requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
            requestDict.setValue(self.contact_id, forKey: "receiver_id")
            requestDict.setValue(msgDict, forKey: "message_data")
            //send socket
            if blockByMe == "0" && blockedMe == "0"{
                socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                self.addToLocal(requestDict: requestDict)
            }else if blockByMe == "1"{
                self.messageTextView.resignFirstResponder()
                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
            }else{
                self.addToLocal(requestDict: requestDict)
            }
            self.messageTextView.text = EMPTY_STRING
        }
        
    }
    
    
    @objc func uploadAudiotoServer(){
        let msgDict = NSMutableDictionary()
        let msg_id = Utility.shared.random()
        msgDict.setValue(UserModel.shared.userID(), forKey: "user_id")
        msgDict.setValue(self.chatDetailDict.value(forKey: "contact_name"), forKey: "user_name")
        msgDict.setValue(msg_id, forKey: "message_id")
        msgDict.setValue("1", forKey: "read_status")
        
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
        
        let result = PHAsset.fetchAssets(withALAssetURLs: [getFileUrl()], options: nil)
        msgDict.setValue(result.firstObject?.localIdentifier, forKey: "local_path")
        let videoData = NSData.init(contentsOf: getFileUrl() as URL)
        
        
        msgDict.setValue("audio", forKey: "message_type")
        msgDict.setValue("audio", forKey: "message")
        var type =  String()
        if (getFileUrl().absoluteString.hasSuffix("m4a")) {
            type = ".m4a"
        } else {
            type = ".mp3"
        }
        self.uploadaudioFiles(msgDict: msgDict, requestDict: requestDict, attachData: videoData! as Data, type:type)
        // socketClass.sharedInstance.uploadaudioFiles(msgDict: msgDict, requestDict: requestDict, attachData: videoData! as Data, type:type,msg_id: msgDict.value(forKey: "message_id") as! String)
        
        
        
    }
    func check_record_permission()
    {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSession.RecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingGranted = true
                } else {
                    self.isAudioRecordingGranted = false
                }
            })
            break
        default:
            break
        }
    }
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        return filePath
    }
    
    
    func setup_recorder()
    {
        if isAudioRecordingGranted
        {
            let session = AVAudioSession.sharedInstance()
            do
            {
                try session.setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                display_alert(msg_title: "Error", msg_desc: error.localizedDescription, action_title: "OK")
            }
        }
        else
        {
            display_alert(msg_title: "Error", msg_desc: "Don't have access to use your microphone.", action_title: "OK")
        }
    }
    
    
    func finishAudioRecording(success: Bool)
    {
        if success
        {
            audioRecorder.stop()
            audioRecorder = nil
        }
        else
        {
            display_alert(msg_title: "Error", msg_desc: "Recording failed.", action_title: "OK")
        }
    }
    
    
    func player(url:NSURL) {
        // Print("playing \(url)")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url as URL)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        } catch let error as NSError {
            //self.player = nil
            // Print(error.localizedDescription)
        } catch {
            // Print("AVAudioPlayer init failed")
        }
        
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool)
    {
        if !flag
        {
            finishAudioRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        record_btn_ref.isEnabled = true
    }
    func display_alert(msg_title : String , msg_desc : String ,action_title : String)
    {
        let ac = UIAlertController(title: msg_title, message: msg_desc, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: action_title, style: .default)
        {
            (result : UIAlertAction) -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        })
        present(ac, animated: true)
    }
    
    
    
    //Mark: uploadaudiotoServer -------------------------------------------------->
    
    
    func uploadaudioFiles(msgDict:NSDictionary,requestDict:NSDictionary,attachData:Data,type:String){
        if Utility.shared.isConnectedToNetwork() {
            let uploadObj = UploadServices()
            uploadObj.uploadFiles(fileData: attachData, type: type, user_id: UserModel.shared.userID()! as String,docuName:msgDict.value(forKey: "message") as! String,msg_id: msgDict.value(forKey: "message_id") as! String,api_type:"private", onSuccess: {response in
                let status:String = response.value(forKey: "status") as! String
                if status == STATUS_TRUE{
                    let time = NSDate().timeIntervalSince1970
                    msgDict.setValue("\(time.rounded().clean)", forKey: "chat_time")
                    
                    let cryptLib = CryptLib()
                    let encryptedMsg = response.value(forKey: "user_image") as? String ?? "" // cryptLib.encryptPlainTextRandomIV(withPlainText:response.value(forKey: "user_image") as? String ?? "", key: ENCRYPT_KEY)

                    msgDict.setValue(encryptedMsg, forKey: "attachment")
                    msgDict.setValue(self.contact_id, forKey: "receiver_id")
                    msgDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
                    msgDict.setValue("\(self.chatDetailDict.value(forKey: "user_id")!)\(UserModel.shared.userID()!)", forKey: "chat_id")
                    msgDict.setValue("0", forKey: "isDownload")
                    msgDict.setValue("single", forKey: "chat_type")
                    
                    requestDict.setValue(msgDict, forKey: "message_data")
                    
                    //send socket
                    if self.blockByMe == "0" && self.blockedMe == "0"{
                        socketClass.sharedInstance.sendMsg(requestDict: requestDict)
                        self.addToLocal(requestDict: requestDict)
                    }else if self.blockByMe == "1"{
                        
                        self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "unblock_send") as? String)
                    }else{
                        self.addToLocal(requestDict: requestDict)
                    }
                }
            })
            dismiss(animated:true, completion: nil)
        } else{
            // self.messageTextView.resignFirstResponder()
            //  self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "check_network") as? String)
        }
    }
    
    
    //Mark:downloadaudiofromServer ------------------------------------------->
    
    func downloadaudiofromserver(index:Int, model :messageModel.message)  {
        
        let msgDict = NSMutableDictionary()
        msgDict.setValue(model.message_data, forKey: "message_data")
        let messageID:String = msgDict.value(forKeyPath: "message_data.message_id") as! String
        let oldMsgDict = NSMutableDictionary.init(dictionary: model.message_data)
        
        self.localDB.updateDownload(msg_id: messageID, status: "2")
        let cell = view.viewWithTag(index + 4000) as? ReceiverVoiceCell
        cell?.loader.play()
        cell?.loader.isHidden = false
        cell?.downloadIcon.isHidden = true
        
        let videoName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(videoName)"),
                let urlData = NSData(contentsOf: url)
            {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/\(videoName)"
                // Print("file path \(filePath)")
                //                DispatchQueue.main.async {
                
                urlData.write(toFile: filePath, atomically: true)
                PhotoAlbum.sharedInstance.saveVideo(url: URL.init(string: filePath)!, msg_id: messageID, type: "single")
                self.localDB.updateDownload(msg_id: messageID, status: "1")
                //oldMsgDict.removeObject(forKey: "isDownload")
                oldMsgDict.setValue("1", forKey: "isDownload")
                let newModel = messageModel.message.init(sender_id: model.sender_id, receiver_id: model.receiver_id, message_data:oldMsgDict, date: model.date)
                self.msgArray.removeObject(at: index)
                self.msgArray.insert(newModel, at: index)
                DispatchQueue.main.async {
                    self.scrollTag = 1
                    self.msgTableView.reloadData()
                }
                //                }
            }
        }
    }
    
    //Mark: download Audio and store the audio to local path -------------------------->
    
    
    func dowloadFile(audioString:String, message: String)  {
        
        if let url = URL.init(string:audioString){
            
            // if let audioUrl = URL(string: "http://freetone.org/ring/stan/iPhone_5-Alarm.mp3") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(message)
            // Print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                // Print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        // Print("File moved to documents folder \(destinationUrl)")
                        
                        
                    } catch let error as NSError {
                        // Print(error.localizedDescription)
                    }
                }).resume()
            }
        }
    }
}
extension ChatDetailPage: RecordViewDelegate {
    
    
    func onStart() {
        if self.counter != 0 {
            self.counter = 0
            self.stopAudioPlayer()
        }
        self.recorderView.isHidden = false
        self.recordView.isHidden = false

        self.attachmentView.isHidden = true
        if(isAudioRecordingGranted) {
            isSwipeCalled = false
            if(!isRecording)
            {
                do {
                    let imageData = try Data(contentsOf: VoiceRecordingSound as URL)
                    audioPlayer_VoiceRecord = try AVAudioPlayer.init(data: imageData)
//                    audioPlayer_VoiceRecord = try AVAudioPlayer(contentsOf: VoiceRecordingSound as URL)
                    audioPlayer_VoiceRecord.play()
                } catch {
                    // Print(error.localizedDescription)
                }
                messageTextView.isHidden = true
                setup_recorder()
                audioRecorder.record()
                isRecording = true
            }
        }
        else{
            self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "microphone_alert") as? String)
        }
        let requestDict = NSMutableDictionary()
        let sender_id:String = UserModel.shared.userID()! as String
        let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String
        requestDict.setValue(sender_id, forKey: "sender_id")
        requestDict.setValue(receiver_id, forKey: "receiver_id")
        requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
        requestDict.setValue("recording", forKey: "type")

        if self.blockedMe != "1" && blockByMe == "0"{
            socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
        }

    }
    
    func onCancel() {
        if(isRecording){
            if(audioRecorder.isRecording){
                audioRecorder.stop()
                audioRecorder.deleteRecording()
            }
        }
        isRecording = false
        let requestDict = NSMutableDictionary()
        requestDict.setValue(UserModel.shared.userID(), forKey: "sender_id")
        requestDict.setValue(self.chatDetailDict.value(forKey: "user_id") as! String, forKey: "receiver_id")
        let sender_id:String = UserModel.shared.userID()! as String
        let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String

        requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
        requestDict.setValue("untyping", forKey: "type")
        if self.blockedMe != "1" && blockByMe == "0"{
            socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
        }
    }
    func updateDuration(duration: CGFloat) {
        let requestDict = NSMutableDictionary()
        let sender_id:String = UserModel.shared.userID()! as String
        let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String
        requestDict.setValue(sender_id, forKey: "sender_id")
        requestDict.setValue(receiver_id, forKey: "receiver_id")
        requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
        requestDict.setValue("recording", forKey: "type")
        recordView.timeLabelText = duration.SecondsFromTimer()
        if self.blockedMe != "1" && blockByMe == "0"{
            socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
        }
//        self.recordView.updateDuration(duration: duration.fromatSecondsFromTimer())
        
    }
    func onFinished(duration: CGFloat) {
         print("end end \(duration)")
        finishAudioRecording(success: true)
        self.recorderView.isHidden = true
        self.recordView.isHidden = true
        if(isRecording)
        {
            self.attachmentView.isHidden = false
            messageTextView.isHidden = false

            ishold = true
            isRecording = false
            if(duration > 0.0){
                self.uploadAudiotoServer()
            }
            else {
                self.view.makeToast(Utility.shared.getLanguage()?.value(forKey: "hold_voice") as? String)
            }
            let requestDict = NSMutableDictionary()
            let sender_id:String = UserModel.shared.userID()! as String
            let receiver_id:String = self.chatDetailDict.value(forKey: "user_id") as! String
            requestDict.setValue(sender_id, forKey: "sender_id")
            requestDict.setValue(receiver_id, forKey: "receiver_id")
            requestDict.setValue("\(receiver_id)\(sender_id)", forKey: "chat_id")
            requestDict.setValue("untyping", forKey: "type")
            if self.blockedMe != "1" && blockByMe == "0"{
                socketClass.sharedInstance.sendTypingStatus(requestDict: requestDict)
            }
        }
        else {
            isRecording = false
        }
    }
    
    func onAnimationEnd() {
        //when Trash Animation is Finished
        print("onAnimationEnd")
        self.recorderView.isHidden = true
        self.recordView.isHidden = true
        self.attachmentView.isHidden = false
        messageTextView.isHidden = false
    }
    
}



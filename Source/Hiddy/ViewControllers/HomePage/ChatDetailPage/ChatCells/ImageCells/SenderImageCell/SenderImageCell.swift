//
//  SenderImageCell.swift
//  Hiddy
//
//  Created by APPLE on 06/06/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit

class SenderImageCell: UITableViewCell {

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var statusIcon: UIImageView!
    @IBOutlet var ImageFileView: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.specificCornerRadius(radius: 15)
        timeLbl.config(color: .white, size: 14, align: .left, text: EMPTY_STRING)
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.timeLbl.textAlignment = .right
            self.ImageFileView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.statusIcon.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else {
            self.timeLbl.transform = .identity
            self.timeLbl.textAlignment = .left
            self.ImageFileView.transform = .identity
            self.statusIcon.transform = .identity
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func config(msgDict:NSDictionary)  {
        let imageName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
        let time:String = msgDict.value(forKeyPath: "message_data.chat_time") as! String
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: time))
        let status:String = msgDict.value(forKeyPath: "message_data.read_status") as! String
        if status == "1"{
            self.statusIcon.image = #imageLiteral(resourceName: "status_sent")
        }else if status == "2"{
            self.statusIcon.image = #imageLiteral(resourceName: "status_notified")
        }else if status == "3"{
            self.statusIcon.image = #imageLiteral(resourceName: "read_tick")
        }
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)")
    }
    func loadImage(url: String) {
//        DispatchQueue.main.async {
            self.ImageFileView.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "profile_placeholder"))
//        }
    }
    func configGroupMsg(model:groupMsgModel.message)  {
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)")
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: model.timestamp))
        self.statusIcon.isHidden = true
    }
    
    func configChannelMsg(model:channelMsgModel.message)  {
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)")
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: model.timestamp))
        self.statusIcon.isHidden = true
    }
}

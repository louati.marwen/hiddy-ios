//
//  MessageCell.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageCell.h"
#import "CryptLib.h"

@interface MessageCell ()
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UIImageView *bubbleImage;
@property (strong, nonatomic) UIImageView *statusIcon;
@property (strong, nonatomic) UIView *bgView;
@end

#define FONT_NAME @"Tajawal-Regular"

@implementation MessageCell

-(CGFloat)height
{
    return _bgView.frame.size.height;
}
-(CGFloat)bgheight
{
    return _bgView.frame.size.height;
}
-(void)updateMessageStatus
{
    [self buildCell];
    //Animate Transition
    _statusIcon.alpha = 0;
    [UIView animateWithDuration:.5 animations:^{
        self->_statusIcon.alpha = 1;
    }];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if ([self.sender_id isEqualToString:self.Own_id]){
        self.bgView.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:234.0/255.0 blue:239.0/255.0 alpha:1.0f];
    }else{
        self.bgView.backgroundColor = [UIColor colorWithRed:21.0/255.0 green:115.0/255.0 blue:137.0/255.0 alpha:1.0f];
    }
}

#pragma mark -

-(id)init{
    
    self = [super init];
    if (self){
        [self commonInit];
    }
    return self;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(void)commonInit{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    _textView = [[UITextView alloc] init];
    _bubbleImage = [[UIImageView alloc] init];
    _timeLabel = [[UILabel alloc] init];
    _statusIcon = [[UIImageView alloc] init];
    _nameLabel = [[UILabel alloc] init];
    _bgView = [[UIView alloc] init];
    _textView.delegate = self;

    _resendButton = [[UIButton alloc] init];
    _resendButton.hidden = YES;
    
    [self.contentView addSubview:_bubbleImage];
    [self.contentView addSubview:_bgView];
    [self.contentView addSubview:_nameLabel];
    [self.contentView addSubview:_textView];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_statusIcon];
    [self.contentView addSubview:_resendButton];
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    _textView.text = @"";
    _timeLabel.text = @"";
    _statusIcon.image = nil;
    _bubbleImage.image = nil;
    _resendButton.hidden = YES;
}


-(void)configCell:(NSDictionary *)msgDict{
//    msgDict.value(forKeyPath: "msgDetails.message_type") as! String
    NSString *timestamp =  [msgDict valueForKeyPath:@"message_data.chat_time"];
    NSDate * new = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"hh:mm a";
    self.time = [dateFormat stringFromDate:new];
//    CryptLib * crypt = [[CryptLib alloc] init];
//    NSString * descriptString = [crypt decryptCipherTextRandomIVWithCipherText:[msgDict valueForKeyPath:@"message_data.message"] key:@"123"];
    self.msgText = [msgDict valueForKeyPath:@"message_data.message"];
    NSString * readStatus ;
    readStatus = [msgDict valueForKeyPath:@"message_data.read_status"];
    if ([readStatus isEqualToString:@"1"]) {
        _statusIcon.image = [self imageNamed:@"status_sent"];
    }else if ([readStatus isEqualToString:@"2"]){
    _statusIcon.image = [self imageNamed:@"status_notified"];
    }else if ([readStatus isEqualToString:@"3"]){
        _statusIcon.image = [self imageNamed:@"status_read"];
    }
    if ( [self.sender_id isEqualToString:self.Own_id]) {
        self.statusIcon.hidden =  FALSE;
    }else{
        self.statusIcon.hidden =  TRUE;
    }
    [self buildCell];
}
-(void)configGroupCell:(NSString *)msg Time:(NSString *)time name:(NSString *)name{
    //    msgDict.value(forKeyPath: "msgDetails.message_type") as! String
    NSDate * new = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"hh:mm a";
    self.time = [dateFormat stringFromDate:new];
    self.msgText = msg;
    self.nameText = name;
    self.statusIcon.hidden =  TRUE;
    [self buildCell];
}

-(void)buildCell
{
    [self setTextView];
    [self setTimeLabel];
//    [self setBubble];
    [self setBacground];

    [self addStatusIcon];
//    [self setStatusIcon];
   
    
    [self setFailedButton];
    [self setNeedsLayout];
}


#pragma mark - TextView

-(void)setTextView{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    if ([self.chat_type isEqualToString:@"group"] && ![self.sender_id isEqualToString:self.Own_id]) {
        [self.nameLabel setHidden:FALSE];
        _nameLabel.frame =  CGRectMake(15, 0, self.contentView.frame.size.width-30, 20);
        _nameLabel.text = self.nameText;
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont fontWithName:FONT_NAME size:17.0];
        _textView.frame = CGRectMake(0, 22, max_witdh, MAXFLOAT);
    }else{
        [self.nameLabel setHidden:TRUE];
        _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
    }
    if ([self.alignment_tag isEqualToString:@"1"]) {
        _nameLabel.textAlignment = NSTextAlignmentRight;
        _nameLabel.transform = CGAffineTransformMakeScale(-1, 1);

    }
    else {
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }

    _textView.font = [UIFont fontWithName:FONT_NAME size:17.0];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.userInteractionEnabled = TRUE;
    _textView.editable = FALSE;
    _textView.scrollEnabled = FALSE;
//    _textView.selectable = FALSE ;
    _textView.dataDetectorTypes = UIDataDetectorTypeAll;
    [UITextView appearance].linkTextAttributes = @{ NSForegroundColorAttributeName : UIColor.orangeColor};
    _textView.text = _msgText;
    [_textView sizeToFit];

    
    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _textView.frame.size.width;
    
    CGFloat textView_h = _textView.frame.size.height;
    UIViewAutoresizing autoresizing;
   if ([self.sender_id isEqualToString:self.Own_id])
    {
        textView_x = self.contentView.frame.size.width - textView_w - 20;
        textView_y = -3;
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        textView_x -= [self isSingleLineCase]?65.0:0.0;
        textView_h = [self isSingleLineCase]?45:_textView.frame.size.height;
        textView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        _textView.textColor = [UIColor blackColor];
    }else{
        textView_x = 20;
        textView_y = -1;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        _textView.textColor = [UIColor whiteColor];
   }
    _textView.autoresizingMask = autoresizing;
   if ([self.chat_type isEqualToString:@"group"] && ![self.sender_id isEqualToString:self.Own_id]) {
       _textView.frame = CGRectMake(textView_x+5, textView_y+25, textView_w+10, textView_h);
   }else {
        if (![self.sender_id isEqualToString:self.Own_id]){
            _textView.frame = CGRectMake(textView_x+5, textView_y+5, textView_w+5, textView_h);
        }else{
            _textView.frame = CGRectMake(textView_x, textView_y+5, textView_w+5, textView_h);
        }
   }
    if ([self.alignment_tag isEqualToString:@"1"]) {
        _textView.textAlignment = NSTextAlignmentRight;
        _textView.transform = CGAffineTransformMakeScale(-1, 1);
    }
    else {
        _textView.textAlignment = NSTextAlignmentLeft;
    }

}
- (BOOL)canBecomeFirstResponder {
    return NO;
}


#pragma mark - TimeLabel

-(void)setTimeLabel
{
    _timeLabel.frame = CGRectMake(0, 0, 52, 14);
    _timeLabel.font = [UIFont fontWithName:FONT_NAME size:13.0];
    _timeLabel.userInteractionEnabled = NO;
    _timeLabel.alpha = 0.7;
    if ([self.alignment_tag isEqualToString:@"1"]) {
        _timeLabel.textAlignment = NSTextAlignmentLeft;
        _timeLabel.transform = CGAffineTransformMakeScale(-1, 1);
    }
    else {
        _timeLabel.textAlignment = NSTextAlignmentRight;
    }

    
    //Set Text to Label
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = YES;
    self.timeLabel.text = self.time;

    //Set position
    CGFloat time_x;
    CGFloat time_y = _textView.frame.size.height - 7;
    
    if ([self.sender_id isEqualToString:self.Own_id])
    {
        time_x = _textView.frame.origin.x + _textView.frame.size.width - _timeLabel.frame.size.width - 20;
        _timeLabel.textColor = [UIColor lightGrayColor];
    }else{
        time_x = MAX(_textView.frame.origin.x + _textView.frame.size.width - _timeLabel.frame.size.width,
                     _textView.frame.origin.x);
        _timeLabel.textColor = [UIColor whiteColor];
    }
    

 
    if ([self isSingleLineCase]){
        time_x = _textView.frame.origin.x + _textView.frame.size.width - 5;
        time_y -= 10;
    }
      if ([self.chat_type isEqualToString:@"group"] && ![self.sender_id isEqualToString:self.Own_id]) {
          _timeLabel.frame = CGRectMake(time_x,time_y+20,_timeLabel.frame.size.width,_timeLabel.frame.size.height);
     }else{
         if ([self.sender_id isEqualToString:self.Own_id]) {
             _timeLabel.frame = CGRectMake(time_x,time_y,_timeLabel.frame.size.width,_timeLabel.frame.size.height);
         }else{
             _timeLabel.frame = CGRectMake(time_x,time_y,_timeLabel.frame.size.width,_timeLabel.frame.size.height);
         }
     }
  
    
    _timeLabel.autoresizingMask = _textView.autoresizingMask;
}
-(BOOL)isSingleLineCase
{
//    CGFloat delta_x = _message.sender == MessageSenderMyself?65.0:44.0;
    CGFloat delta_x ;
    if ([self.sender_id isEqualToString:self.Own_id]){
        delta_x = 65.0;
    }else{
        delta_x = 44.0;
    }


    CGFloat textView_height = _textView.frame.size.height;
    CGFloat textView_width = _textView.frame.size.width;
    CGFloat view_width = self.contentView.frame.size.width;
    
    //Single Line Case
    return (textView_height <= 45 && textView_width + delta_x <= 0.8*view_width)?YES:NO;
}

#pragma mark - Bubble

- (void)setBacground
{
    //Margins to Bubble
    CGFloat marginLeft = 0;
    CGFloat marginRight = 2;
    self.bgView.layer.cornerRadius = 15;
    self.bgView.clipsToBounds = TRUE;
    
    //Bubble positions
    CGFloat bubble_x;
    CGFloat bubble_y = 0;
    CGFloat bubble_width;
    CGFloat bubble_height = MIN(_textView.frame.size.height + 13,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
    if ([self.sender_id isEqualToString:self.Own_id]){
        bubble_x = MIN(_textView.frame.origin.x -marginLeft,_textView.frame.origin.x - 2*marginLeft);
        bubble_width = self.contentView.frame.size.width - bubble_x - marginRight;
        bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
        bubble_x -= 15;

    }else{
        bubble_x = marginRight+10;
        bubble_width = MAX(_textView.frame.origin.x + _textView.frame.size.width + marginLeft,
                           _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
//        bubble_x += 10;

    }
    
    
    
    if ([self.chat_type isEqualToString:@"group"] && ![self.sender_id isEqualToString:self.Own_id]) {
        if ([self isSingleLineCase]){
            _bgView.frame = CGRectMake(bubble_x, bubble_y+20, bubble_width-5, bubble_height-12);
        }else{
            _bgView.frame = CGRectMake(bubble_x, bubble_y+20, bubble_width-5, bubble_height);
        }
    }else{
        if ([self.sender_id isEqualToString:self.Own_id]){
            _bgView.frame = CGRectMake(bubble_x+5, bubble_y, bubble_width+5, bubble_height);
        }else{
            _bgView.frame = CGRectMake(bubble_x+5, bubble_y, bubble_width-10, bubble_height);
        }
    }
    _bgView.autoresizingMask = _textView.autoresizingMask;
    
   }

- (void)setBubble{
    //Margins to Bubble
    CGFloat marginLeft = 5;
    CGFloat marginRight = 2;
    //Bubble positions
    CGFloat bubble_x;
    CGFloat bubble_y = 0;
    CGFloat bubble_width;
    CGFloat bubble_height = MIN(_textView.frame.size.height + 13,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
    if ([self.sender_id isEqualToString:self.Own_id]){
        bubble_x = MIN(_textView.frame.origin.x -marginLeft,_textView.frame.origin.x - 2*marginLeft);
        
        _bubbleImage.image = [[self imageNamed:@"bubbleMine"]
                              stretchableImageWithLeftCapWidth:15 topCapHeight:14];
        bubble_width = self.contentView.frame.size.width - bubble_x - marginRight;
        bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
    }
    else
    {
        bubble_x = marginRight;
        
        _bubbleImage.image = [[self imageNamed:@"bubbleSomeone"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        bubble_width = MAX(_textView.frame.origin.x + _textView.frame.size.width + marginLeft,
                           _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
    }
    
    if ([self.chat_type isEqualToString:@"group"] && ![self.sender_id isEqualToString:self.Own_id]) {
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y+20, bubble_width, bubble_height);
    }else{
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width, bubble_height);
    }
    _bubbleImage.autoresizingMask = _textView.autoresizingMask;
}

#pragma mark - StatusIcon

-(void)addStatusIcon
{/*
    CGRect textFrame = _textView.frame;
    CGFloat view_width = self.contentView.frame.size.width;
    CGRect status_frame = CGRectMake(0, 0, 12, 12);
    if ([self.sender_id isEqualToString:self.Own_id]){
        status_frame.origin.x = view_width-80;
    }else{
        status_frame.origin.x = 10;
    }
    status_frame.origin.y = textFrame.size.height+3;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeScaleAspectFill;
    _statusIcon.autoresizingMask = _textView.autoresizingMask; */
    CGRect time_frame = _timeLabel.frame;
    CGRect status_frame = CGRectMake(0, 0, 10, 10);
    status_frame.origin.x = time_frame.origin.x + time_frame.size.width + 5;
    status_frame.origin.y = time_frame.origin.y;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeScaleAspectFill;
    _statusIcon.autoresizingMask = _textView.autoresizingMask;
}
-(void)setStatusIcon{
    if (self.status == MsgStatusSending)
        _statusIcon.image = [self imageNamed:@"status_sending"];
    else if (self.status == MsgStatusSent)
        _statusIcon.image = [self imageNamed:@"status_sent"];
    else if (self.status == MsgStatusReceived)
        _statusIcon.image = [self imageNamed:@"status_notified"];
    else if (self.status == MsgStatusRead)
        _statusIcon.image = [self imageNamed:@"status_read"];
    if (self.status == MsgStatusFailed)
        _statusIcon.image = nil;
    if ( ![self.sender_id isEqualToString:self.Own_id]) {
        _statusIcon.hidden = TRUE;
    }
}

#pragma mark - Failed Case

//
// This delta is how much TextView
// and Bubble should shit left
//
-(NSInteger)fail_delta{
    return 60;
}
-(BOOL)isStatusFailedCase
{
    return self.status == MsgStatusFailed;
}
-(void)setFailedButton
{
    NSInteger b_size = 22;
    CGRect frame = CGRectMake(self.contentView.frame.size.width - b_size - [self fail_delta]/2 + 5,
                              (self.contentView.frame.size.height - b_size)/2,
                              b_size,
                              b_size);
    _resendButton.frame = frame;
    _resendButton.hidden = ![self isStatusFailedCase];
    [_resendButton setImage:[self imageNamed:@"status_failed"] forState:UIControlStateNormal];
}

#pragma mark - UIImage Helper

-(UIImage *)imageNamed:(NSString *)imageName
{
    return [UIImage imageNamed:imageName
                      inBundle:[NSBundle bundleForClass:[self class]]
 compatibleWithTraitCollection:nil];
}

@end

//
//  MessageCell.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>

//
// This class build bubble message cells
// for Income or Outgoing messages
//

typedef NS_ENUM(NSInteger, MsgStatus)
{
    MsgStatusSending,
    MsgStatusSent,
    MsgStatusReceived,
    MsgStatusRead,
    MsgStatusFailed
};

@interface MessageCell : UITableViewCell<UITextViewDelegate>

@property (strong, nonatomic) NSString *msgText;
@property (strong, nonatomic) NSString *nameText;
@property (strong, nonatomic) NSString *time;
@property (assign, nonatomic) MsgStatus status;

@property (strong, nonatomic) NSString * sender_id;
@property (strong, nonatomic) NSString * alignment_tag;
@property (strong, nonatomic) NSString * Own_id;
@property (strong, nonatomic) NSString * chat_type;

@property (strong, nonatomic) CAGradientLayer *gradient;

@property (strong, nonatomic) UIButton *resendButton;

-(void)updateMessageStatus;
-(void)configCell:(NSDictionary *)msgDict;
-(void)configGroupCell:(NSString *)msg Time:(NSString *)time name:(NSString *)name;

//Estimate BubbleCell Height
-(CGFloat)height;
-(CGFloat)bgheight;

@end

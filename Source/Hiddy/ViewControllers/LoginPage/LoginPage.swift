//
//  LoginPage.swift
//  Hiddy
//
//  Created by APPLE on 07/06/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit
import FirebaseUI
import PhoneNumberKit

class LoginPage: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var processingLabel: UILabel!

    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    @IBOutlet weak var hideView: UIView!
    var enableSendToFacebook = Bool()
    var enableGetACall = Bool()
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageIndicator: UIPageControl!
    @IBOutlet var connectBtn: UIButton!
    var isViewed = 0

    let imagelist = ["sliderImg1", "sliderImg2", "sliderImg3"]
    let sliderTitleList = ["channel", "group_event", "audio_video"]
    let sliderDesList = ["channel_des", "group_event_des", "audio_video_des"]

    var yPosition:CGFloat = 0
    var scrollViewContentSize:CGFloat=0;
    
    let authUI = FUIAuth.defaultAuthUI()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityindicator.color = RECIVER_BG_COLOR
        self.processingLabel.config(color:RECIVER_BG_COLOR, size: 16, align: .natural, text: "processing")
        authUI?.delegate = self

        let providers: [FUIAuthProvider] = [
            FUIPhoneAuth(authUI:FUIAuth.defaultAuthUI()!),
            ]
        self.authUI?.providers = providers
        // Do any additional setup after loading the view.
    }
    func firebaseLogin() {
        let phoneProvider = FUIAuth.defaultAuthUI()?.providers.first as! FUIPhoneAuth
        phoneProvider.signIn(withPresenting: self, phoneNumber: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override  func viewDidLayoutSubviews() {
        self.view.applyGradient()
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        if self.isViewed == 0 {
            self.hideView.isHidden = true
            self.activityindicator.stopAnimating()
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    func initialSetup()  {
        self.configurePageControl()
        self.configSliderPage()
        self.connectBtn.config(color: .white, size: 22, align: .center, title: "connect")
        self.connectBtn.cornerRoundRadius()
        self.connectBtn.backgroundColor  =  UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(pageIndicator)
        self.view.bringSubviewToFront(connectBtn)
        self.view.bringSubviewToFront(hideView)
    }
   
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageIndicator.numberOfPages = imagelist.count
        self.pageIndicator.currentPage = 0
        self.pageIndicator.tintColor = UIColor.white
        self.pageIndicator.pageIndicatorTintColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        self.pageIndicator.currentPageIndicatorTintColor =  .white
    }
    
    func configSliderPage()  {
        for  i in stride(from: 0, to: imagelist.count, by: 1) {
            var frame = CGRect.zero
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(i)
            frame.origin.y = 0
            frame.size = self.scrollView.frame.size
            self.scrollView.isPagingEnabled = true
            
            let myImage:UIImage = UIImage(named: imagelist[i])!
            let myImageView:UIImageView = UIImageView()
            myImageView.image = myImage
            myImageView.tag = i
            myImageView.contentMode = UIView.ContentMode.scaleAspectFit
            myImageView.frame = CGRect.init(x: frame.origin.x+50, y: 80, width: FULL_WIDTH-100, height: FULL_WIDTH-100)
            scrollView.addSubview(myImageView)
            
            let titleLbl:UILabel = UILabel()
            titleLbl.frame = CGRect.init(x: frame.origin.x, y: FULL_WIDTH, width: FULL_WIDTH, height:45)
            titleLbl.config(color: .white, size: 25, align: .center, text: sliderTitleList[i])
            scrollView.addSubview(titleLbl)
            
            let titleDes:UILabel = UILabel()
            titleDes.frame = CGRect.init(x: frame.origin.x+20, y: FULL_WIDTH+40, width: FULL_WIDTH-40, height:60)
            titleDes.numberOfLines = 4
            titleDes.config(color: .white, size: 17, align: .center, text: sliderDesList[i])
            scrollView.addSubview(titleDes)
        }
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(imagelist.count), height: self.scrollView.frame.size.height-100)
    }
    
    //scroll view delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width) 
        pageIndicator.currentPage = Int(pageNumber)
    }
    
    @IBAction func connectBtnTapped(_ sender: Any) {
        self.hideView.isHidden = false
        self.activityindicator.startAnimating()
        self.isViewed = 1
        self.firebaseLogin()
//        self.loginService(phoneNo: "8124206746", countryCode: "+91", country_name: "IN")
    }
    
    //login web service
    func loginService(phoneNo:String,countryCode:String,country_name: String){
        let loginObj = UserWebService()
        loginObj.signUpService(user_name:EMPTY_STRING , phone_no:phoneNo , country_code: countryCode,country_name: country_name, onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                let detailObj = DetailsPage()
                detailObj.userDict = response
                self.isViewed = 0
                self.navigationController?.pushViewController(detailObj, animated: true)
            }
        })
    }
}
extension LoginPage: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        print(error?.localizedDescription ?? "")
        if error == nil {
            let phoneNumberKit = PhoneNumberKit()
            do {
                let phoneNumbers = try phoneNumberKit.parse(authDataResult?.user.phoneNumber ?? "")
                self.loginService(phoneNo: "\(phoneNumbers.nationalNumber)", countryCode: "\(phoneNumbers.countryCode)", country_name: (phoneNumbers.regionID ?? ""))
            }
            catch {
                self.isViewed = 0
                print("Generic parser error")
            }
        }
        else {
            self.isViewed = 0
        }
    }
    
    func authUI(_ authUI: FUIAuth, didFinish operation: FUIAccountSettingsOperationType, error: Error?) {
        print(error?.localizedDescription ?? "")
        self.isViewed = 1
    }
    
}

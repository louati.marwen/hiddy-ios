//
//  ChooseLanguage.swift
//  Hiddy
//
//  Created by APPLE on 16/07/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit
protocol languageDelegate {
    func selectedLanguage(language:String)
}

class ChooseLanguage: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var selectLbl: UILabel!
    @IBOutlet var languageTableView: UITableView!
    var languageArray = NSMutableArray()
    var selectedLanguage = String()
    var delegate:languageDelegate?
    var languageCodeArr = ["en","fr","ar"]
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var navigationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        self.changeRTLView()
    }
    func changeRTLView() {
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.textAlignment = .right
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.selectLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.selectLbl.textAlignment = .right
        }
        else {
            self.view.transform = .identity
            self.titleLbl.textAlignment = .left
            self.titleLbl.transform = .identity
            self.selectLbl.transform = .identity
            self.selectLbl.textAlignment = .left
        }
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initialSetup()  {
        self.navigationView.elevationEffect()
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, size: 20, align: .left, text: "app_language")
        if !IS_IPHONE_X {
            self.titleLbl.frame = CGRect(x: self.titleLbl.frame.origin.x, y: self.titleLbl.frame.origin.y, width: self.titleLbl.frame.width, height: 30)
            self.navigationView.frame = CGRect(x: self.navigationView.frame.origin.x, y: self.navigationView.frame.origin.y, width: self.navigationView.frame.width, height:70)
        }
        self.selectLbl.config(color: TEXT_PRIMARY_COLOR, size: 17, align: .left, text: "select_language")
        languageTableView.register(UINib(nibName: "languageCell", bundle: nil), forCellReuseIdentifier: "languageCell")
        languageArray = ["English","Française", "عربى"]
        self.languageTableView.reloadData()
        self.selectedLanguage = UserModel.shared.getAppLanguage()!
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int{
        return self.languageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as! languageCell
        let language :String = self.languageArray.object(at: indexPath.row) as! String
        cell.languageNameLbl.text = language
        
        if self.selectedLanguage == language{
            cell.selctionView.applyGradient()
        }else if self.selectedLanguage != language{
            cell.selctionView.removeGrandient()
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.loader.startAnimating()
        let language :String = self.languageArray.object(at: indexPath.row) as! String
        self.selectedLanguage = language
        UserModel.shared.LANGUAGE_CODE = self.languageCodeArr[indexPath.row]
        self.delegate?.selectedLanguage(language: language)
        self.dismiss(animated: true, completion: nil)
        }
}
